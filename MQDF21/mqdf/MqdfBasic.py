import numpy as np
import math as m


class MqdfBasic:
    def __init__(self):
        pass

    def cov_matrix(self, a0, opt=1):
        n = len(a0)
        m_ones = np.ones(n)
        a = a0 - np.dot(m_ones, a0) / n
        v = np.dot(np.transpose(a), a) / (n - opt)
        return v

    def np_transpose(self, a0):
        return np.transpose(a0)

    def np_dot(self, a0,a1):
        return np.dot(a0,a1)

    def norm_euc(self, a0):
        return np.linalg.norm(a0)

    def inv_matrix(self, a0):
        return np.linalg.inv(a0)

    def mean_class(self, a0):
        return np.mean(a0, axis=0)

    def det(self, a0):
        return np.linalg.det(a0)

    def log(self, n):
        n = abs(n)
        return m.log10(n)

    def calc_prior_probabilities(self, local_list_group):
        prior_probabilities = []
        size = 0
        for group in local_list_group:
            c = len(group)
            prior_probabilities.append(c)
            size = size + c
        for loc_i in range(len(prior_probabilities)):
            n = prior_probabilities[loc_i]
            phi = n / size
            log_phi = self.log(phi)
            prior = [n, phi, log_phi]
            prior_probabilities[loc_i] = prior
        return prior_probabilities

    def calc_eigen_val_vec(self, a0):
        eig_val, eig_vec = np.linalg.eig(a0)
        sorted_eig_val = np.sort(eig_val)[::-1]
        n = len(eig_val)
        sorted_eig_vec = np.zeros(shape=(n, 0))
        for i in range(n):
            for j in range(n):
                if sorted_eig_val[i] == eig_val[j]:
                    eve = eig_vec[:, j].T.reshape(n, 1)
                    sorted_eig_vec = np.append(sorted_eig_vec, eve, axis=1)
                    break
        # new_sorted_eig_vec = sorted_eig_vec.T
        return sorted_eig_val, sorted_eig_vec
