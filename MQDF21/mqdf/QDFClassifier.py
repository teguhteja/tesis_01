import mqdf.W as W
import numpy as np
import mqdf
import time as t
import math as m


class QDFClassifier:

    def __init__(self, class_w, group_name):
        self.class_w = class_w
        self.group_name = group_name

    @staticmethod
    def log(n):
        n = abs(n)
        return m.log10(n)

    def mqdf_classifier(self, x_te, y_te, d, mode_loop=True, st_d=1):
        p_class_w = len(self.class_w)
        p_x_te = len(x_te)
        pb = mqdf.ProgressBar(p_x_te)
        s_a = ""
        if mode_loop:
            r_d = range(st_d, d+1)
        else:
            r_d = range(d, d+1)
        for k in r_d:
            start2 = t.time()
            count = 0
            for j in range(p_x_te):
                pb.print(j, s_a)
                # TODO: uncomment after check
                x = x_te[j]
                y = y_te[j][0]
                name_result = ""
                acc_temp = float("inf")
                for i in range(p_class_w):
                    w = self.class_w[i]
                    # if w.a_sum is None:
                    #     temp = [None]*p_x_te
                    #     temp1 = [None]*p_x_te
                    #     temp2 = [None]*p_x_te
                    #     w.a_sum = temp
                    #     w.a_xs_mean = temp1
                    #     w.a_r1 = temp2
                    g2_7 = self.mqdf2_7_c(x, w, k, j)
                    if g2_7 < acc_temp:
                        name_result = w.name
                        acc_temp = g2_7
                if name_result == y:
                    count += 1
                accuracy = count / p_x_te * 100
                s_a = "a= " + str(round(accuracy,2)) + "%"
            if accuracy == 100.0:
                j = p_x_te
                k = d
            end2 = t.time()
            proses = end2 - start2
            print("MQDF2_7 k=", k, "; a=", round(accuracy, 4), "%; time=", round(proses, 4))
            return name_result

    # i = idx class w, j = idx testing, k = bound.
    @staticmethod
    def mqdf2_7(self, x, w, k, j):
        if w.a_xs_mean[j] is None:
            x_s_m = x - w.mean
            w.a_xs_mean[j] = x_s_m
            norm = np.linalg.norm(x_s_m)
            r1 = norm**2
            w.a_r1[j] = r1
        else:
            x_s_m = w.a_xs_mean[j]
            r1 = w.a_r1[j]
        sum_1 = 0.0
        sum_3 = 0.0
        delta = w.minor_eig_val(k)
        if w.a_sum[j] is not None:
            prev_val_sum = w.a_sum[j]
            sum_1 = prev_val_sum[0]
            sum_3 = prev_val_sum[1]
            r1 = prev_val_sum[2]
        curr = k - 1
        eig_val = w.eig_val[curr]
        eig_vec = w.eig_vec[:, curr]
        sum_1_1 = np.dot(np.transpose(eig_vec), x_s_m)
        sum_1 = sum_1 + 1 / eig_val * sum_1_1 ** 2
        r1 = r1 - sum_1_1 ** 2
        sum_3 = sum_3 + m.log10(eig_val)
        w.a_sum[j] = (sum_1, sum_3, r1)
        sum_4 = (w.d - k) * m.log10(delta)
        g2_7 = sum_1 + 1 / delta * r1 + sum_3 + sum_4
        return g2_7

    def mqdf2_7_c(self, x, w, k, j):
        x_s_m = x - w.mean
        norm = np.linalg.norm(x_s_m)
        r1 = norm ** 2
        # delta = w.minor_eig_val(k)
        delta = w.eig_val_min
        eig_val = w.eig_val[:k]
        seper = np.power(eig_val, -1.0)
        eig_vec = w.eig_vec[:, :k]
        sum_1_1 = np.dot(np.transpose(eig_vec), x_s_m)
        sum_1_2 = np.power(sum_1_1, 2.0)
        sum_1_3 = np.dot(np.transpose(seper), sum_1_2)
        if isinstance(sum_1_3, np.ndarray):
            slice_sum_1_3 = sum_1_3[0:k]
        else:
            slice_sum_1_3 = sum_1_3
        sum_1 = np.sum(slice_sum_1_3)
        slice_sum_2_2 = sum_1_2[0:k]
        sum_2_2 = np.sum(slice_sum_2_2)
        sum_2 = 1/delta * (r1-sum_2_2)
        sum_3_1 = np.log10(eig_val)
        slice_sum_3_1 = sum_3_1[0:k]
        sum_3 = np.sum(slice_sum_3_1)
        # sum_4 = (w.d - k) * m.log10(delta)
        g2_7 = sum_1 + sum_2 + sum_3
        # print("i: ", j, "; gn:", w.name, "; ", round(g2_7,2), "= ", round(sum_1,2), "+ ", round(sum_2,2), "+ ",
        #       round(sum_3,2), "+ ", round(sum_4,2) )
        return g2_7
