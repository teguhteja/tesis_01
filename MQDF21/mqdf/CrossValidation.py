import pandas as pd
import numpy as np
# import mqdf


class CrossValidation:
    def __init__(self, xy_sel, x_te, y_te, gr_nm):
        self.xy_cv = xy_sel
        self.x_te = x_te
        self.y_te = y_te
        self.gr_nm = gr_nm
        self.x_te_exc = []
        self.y_te_exc = []

    #true = bootstrap, false = Cross Validation
    def sub_sampling(self, number, b_val=False):
        Pb = mqdf.ProgressBar(len(self.gr_nm))
        j=0
        for name in self.gr_nm:
            Pb.print(j); j += 1
            # TODO: uncomment after check
            idx_range, temp = np.where(self.y_te == name)
            n_smp = int(number*len(idx_range))
            indexes = np.random.choice(idx_range, size=n_smp, replace=b_val)
            sample = np.concatenate((self.x_te[indexes], self.y_te[indexes]), axis=1)
            sample_df = pd.DataFrame(data=sample,  columns=self.xy_cv.columns)
            self.xy_cv = self.xy_cv.append(sample_df, ignore_index=True)
            idx_exclude = idx_range[~np.isin(idx_range, indexes)]
            if len(self.x_te_exc) == 0:
                self.x_te_exc = self.x_te[idx_exclude]
                self.y_te_exc = self.y_te[idx_exclude]
            else:
                self.x_te_exc = np.concatenate((self.x_te_exc,self.x_te[idx_exclude]), axis=0)
                self.y_te_exc = np.concatenate((self.y_te_exc,self.y_te[idx_exclude]), axis=0)
        print("Subsample ratio ", number, "bootstrap status :", b_val)

    def get_xy_cv(self):
        return self.xy_cv

    def get_xy_exclude(self):
        return self.x_te_exc, self.y_te_exc
