import pandas as pd
import numpy as np
import mqdf


class SplitData:
    def __init__(self, my_xy_tr, tr_x, tr_y, my_group_name_tr):
        self.my_xy_tr = my_xy_tr
        self.gr_nm = my_group_name_tr
        self.tr_x = tr_x
        self.tr_y = tr_y
        self.x_te_exc = []
        self.y_te_exc = []

    def process(self, number,b_val=False):
        Pb = mqdf.ProgressBar(len(self.gr_nm))

        my_split_xy_c = []
        my_split_x_tr = []
        my_split_y_tr = []
        x_tr_exc = []
        y_tr_exc = []
        x_tr_now = []
        y_tr_now = []
        ratio = 1/number
        for i in range(number):
            j = 0
            df = pd.DataFrame()
            x_tr_inc = []
            y_tr_inc = []
            if i == 0 :
                x_tr_now = self.tr_x
                y_tr_now = self.tr_y
            else :
                x_tr_now = x_tr_exc
                y_tr_now = y_tr_exc
            for name in self.gr_nm:
                Pb.print(j); j += 1
                # TODO: uncomment after check
                idx_range1, temp = np.where(self.tr_y == name)
                n_smp = int(ratio * len(idx_range1))
                idx_range, temp = np.where(y_tr_now == name)
                indexes = np.random.choice(idx_range, size=n_smp, replace=b_val)
                sample = np.concatenate((x_tr_now[indexes], y_tr_now[indexes]), axis=1)
                sample_df = pd.DataFrame(data=sample,  columns=self.my_xy_tr.columns)
                df = df.append(sample_df, ignore_index=True)
                idx_exclude = idx_range[~np.isin(idx_range, indexes)]
                # x_tr_exc.append(x_tr_now[idx_exclude])
                # y_tr_exc.append(y_tr_now[idx_exclude])
                # x_tr_inc.append(x_tr_now[indexes])
                # y_tr_inc.append(y_tr_now[indexes])
                if len(x_tr_exc) == 0:
                    x_tr_exc = x_tr_now[idx_exclude]
                    y_tr_exc = y_tr_now[idx_exclude]
                else:
                    x_tr_exc = np.concatenate((x_tr_exc,x_tr_now[idx_exclude]), axis=0)
                    y_tr_exc = np.concatenate((y_tr_exc,y_tr_now[idx_exclude]), axis=0)
                if len(x_tr_inc) == 0:
                    x_tr_inc = x_tr_now[indexes]
                    y_tr_inc = y_tr_now[indexes]
                else:
                    x_tr_inc = np.concatenate((x_tr_inc,x_tr_now[indexes]), axis=0)
                    y_tr_inc = np.concatenate((y_tr_inc,y_tr_now[indexes]), axis=0)
            my_split_xy_c.append(df)
            my_split_x_tr.append(x_tr_inc)
            my_split_y_tr.append(y_tr_inc)
        # print(str(len(my_split_xy_c))+","+str(len(my_split_x_tr))+","+str(len(my_split_y_tr)))
        return my_split_xy_c, my_split_x_tr, my_split_y_tr
