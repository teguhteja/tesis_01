import math as m
import time as t
import numpy as np
import mqdf.ProgressBar
import mqdf.MQDF3Classifier as mqdf3

class BuildW :

    def __init__(self, class_w):
        self.class_w = class_w
        self.s_sigma_o = None
        self.s_tau_o = None
        self.s_mu_o = None

    def set_param_dlqdf(self, s_sigma_o, s_tau_o, s_mu_o):
        self.s_sigma_o = s_sigma_o
        self.s_tau_o = s_tau_o
        self.s_mu_o = s_mu_o

    def build_it(self, x, y):
        p_class_w = len(self.class_w)
        p_x_te = len(x)
        start2 = t.time()
        count = 0
        pb2 = mqdf.ProgressBar(p_class_w)
        accuracy = 0.0
        s_a = ""
        for i in range(p_class_w):
            pb2.print(i)
            w_ = self.class_w[i]
            sel_nm = w_.name
            idx, temp = np.where(y == sel_nm)
            p_matrix = len(idx)
            m_g2 = np.zeros((p_matrix, p_matrix))
            row = 0
            for j0 in idx:
                w_.eig_val = np.power(m.e, self.s_sigma_o[j0][i])
                w_.eig_val_min = np.power(m.e, self.s_tau_o[j0][i])
                w_.mean = self.s_mu_o[j0][i]
                col = 0
                for j1 in idx:
                    x_j = x[j1]
                    g2_7 = mqdf3.mqdf2_7_c(mqdf3, x_j, w_)
                    # print(i, "-", row, "-", col)
                    m_g2[row][col] = g2_7
                    col += 1
                row += 1
            sum_m_g2 = np.sum(m_g2, axis=1)
            idx_min = np.argmin(sum_m_g2)
            w_.eig_val = np.power(m.e, self.s_sigma_o[idx_min][i])
            w_.eig_val_min = np.power(m.e, self.s_tau_o[idx_min][i])
            w_.mean = self.s_mu_o[idx_min][i]
        pb2 = mqdf.ProgressBar(p_x_te)
        for j in range(p_x_te):
            x_ = x[j]
            y_ = y[j][0]
            pb2.print(j, s_value=s_a)
            name_result = ""
            acc_temp = float("inf")
            for i in range(p_class_w):
                w_ = self.class_w[i]
                g2_7 = mqdf3.mqdf2_7_c(mqdf3, x_, w_)
                if g2_7 < acc_temp:
                    name_result = w_.name
                    acc_temp = g2_7
            if name_result == y_:
                count += 1
            accuracy = count / p_x_te * 100
            s_a = "a=" + str(round(accuracy, 2)) + "%"
        end2 = t.time()
        proses = end2 - start2
        title = "MQDF2_7"
        print(title, "a=", round(accuracy, 4), "%; time=", round(proses, 2))
