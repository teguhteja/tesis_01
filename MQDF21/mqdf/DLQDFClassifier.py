import math as m
import time as t
import numpy as np
import mqdf.ProgressBar


class DLQDFClassifier:
    __slots__ = ['class_w', 'group_name', 'xi', 'alpha', 'n_slice', 'p_vec', 'j', 'is_print', 'lr1', 'lr2', 'lr3',
                 's_sigma_o', 's_tau_o', 's_mu_o', 'iteration', 'minor_eig_val', 'all_k', 'log_process', 'is_bounded','ind_r', 'my_i' ]

    def __init__(self, class_w, group_name, p_vec, n_slice, log_process, is_bounded=True):
        self.class_w = class_w
        self.group_name = group_name
        self.all_k = None
        self.minor_eig_val = None
        self.xi = None  # xi
        self.alpha = None
        if n_slice == 0 or n_slice is None:
            self.n_slice = p_vec
        else:
            self.n_slice = n_slice  # d / number of eigen vector
        self.p_vec = p_vec
        self.j = None
        self.is_print = False
        self.lr1 = None
        self.lr2 = None
        self.lr3 = None
        self.iteration = 0
        self.is_bounded = is_bounded

        self.s_sigma_o = None
        self.s_tau_o = None
        self.s_mu_o = None
        self.ind_r = 0
        self.my_i = 0

        self.log_process = log_process

    def set_xi_alpha(self, xi=0.0, alpha=0.0):
        self.xi = xi
        self.alpha = alpha

    def set_lr(self, lr1, lr2, lr3):
        self.lr1 = lr1
        self.lr2 = lr2
        self.lr3 = lr3

    def set_it(self, it):
        self.iteration = it

    def get_w_dlqdf(self):
        return self.class_w

    def get_param_dlqdf2(self):
        return self.s_sigma_o, self.s_tau_o, self.s_mu_o

    def set_mev(self, minor_eig_val):
        self.minor_eig_val = minor_eig_val

    def set_all_k(self, all_k):
        self.all_k = all_k

    def set_print(self, is_print):
        self.is_print = is_print

    def init_param(self, p_x_te, p_class_w):
        self.s_sigma_o = np.zeros((p_x_te, p_class_w, self.n_slice))
        self.s_tau_o = np.zeros((p_x_te, p_class_w, 1))
        self.s_mu_o = np.zeros((p_x_te, p_class_w, self.p_vec))
        s_sigma = np.zeros((p_x_te, p_class_w, self.n_slice))
        s_tau = np.zeros((p_x_te, p_class_w, 1))
        s_mu = np.zeros((p_x_te, p_class_w, self.p_vec))
        hc_x = np.zeros(p_x_te)
        ind_c = np.zeros(p_x_te)
        return hc_x, ind_c, s_sigma, s_tau, s_mu

    def check_is_bounded(self, new_accuracy, old_accuracy):
        if self.is_bounded:
            return 99.5 > new_accuracy >= old_accuracy
        else:
            return True

    def dlqdf_classifier(self, x_te, y_te):
        p_class_w = len(self.class_w)  # i
        p_x_te = len(x_te)  # j
        pb2 = mqdf.ProgressBar(p_x_te)
        hc_x, ind_c, s_sigma, s_tau, s_mu = self.init_param(p_x_te, p_class_w)
        it = 0
        is_dlqdf = False
        is_dlqdf2 = True
        bound_iteration = self.iteration
        accuracy = 0.0
        accuracy_0 = 0.0
        s_mev = round(self.minor_eig_val, 4) if self.minor_eig_val is not None else "Calc MEV in DLQDF"
        # print("xi=", self.xi, "alpha=", self.alpha, "lr1, lr3=", self.lr1, "lr2=", str(round(self.lr2, 8)),
        #       "mev=", s_mev)
        msg = f"xi= {self.xi} alpha= {self.alpha} lr1, lr3 =  {self.lr1} lr2 =  {str(round(self.lr2, 8))} mev= {s_mev}"
        # print(msg)
        self.log_process(msg=msg)
        name_result = ""
        ind_r = 0
        ind_r_f = 0
        my_i = 0
        while it < bound_iteration and (self.check_is_bounded(accuracy, accuracy_0) or is_dlqdf2):
            start2 = t.time()
            count = 0
            l1 = 0.0
            p_1 = "Calc DLQDF:" if is_dlqdf else "Calc MQDF:"
            print(p_1, it, end="")
            accuracy_0 = accuracy
            for j in range(p_x_te):
                self.j = j
                x = x_te[j]
                y = y_te[j][0]
                dq_temp = float("inf")
                dq_c = 0.0
                dq_r = float("inf")
                s_print_r = ""
                # s_print = ""
                for i in range(p_class_w):
                    w = self.class_w[i]
                    if is_dlqdf is False:
                        w.eig_val_min = self.minor_eig_val if self.minor_eig_val is not None else w.eig_val_min
                        self.s_sigma_o[j][i] = w.eig_val[:self.n_slice]
                        self.s_tau_o[j][i] = w.eig_val_min
                        self.s_mu_o[j][i] = w.mean
                    else:
                        sigma_t1, tau_t1, mu_t1 = self.update_sgd_01(ind_c[j] == i, hc_x[j], self.s_sigma_o[j][i],
                                                                     self.s_tau_o[j][i], self.s_mu_o[j][i],
                                                                     s_sigma[j][i], s_tau[j][i], s_mu[j][i])
                        # if ind_c[j] == i or ind_r_f == i:
                        #     s_print = f"\nclass update : {w.name} tau_t1={tau_t1} sigma_t1={sigma_t1[:6]} mu_t1={mu_t1[:6]}"
                        #     self.log_process(msg=s_print)
                        self.s_sigma_o[j][i] = sigma_t1
                        self.s_tau_o[j][i] = tau_t1
                        self.s_mu_o[j][i] = mu_t1
                        try:
                            w.eig_val = np.power(m.e, sigma_t1)
                        except RuntimeWarning:
                            print(f"{w.eig_val} = power({m.e},{sigma_t1})")
                        w.eig_val_min = np.power(m.e, tau_t1)
                        w.mean = mu_t1
                    dq_13, s_tau_0, s_sigma_0, s_mu_0, s_print = self.mqdf2_7_01(x, w, is_dlqdf)
                    if w.name == y:
                        dq_c = dq_13
                        ind_c[j] = i
                        my_i = i
                        if self.is_print :
#                             print(s_print)
                            self.log_process(msg=s_print)
                    if dq_r > dq_13 and w.name != y:
                        dq_r = dq_13
                        s_print_r = s_print
                        ind_r = i
                    if dq_temp > dq_13:
                        name_result = w.name
                        dq_temp = dq_13
                    s_tau[j][i] = s_tau_0
                    s_sigma[j][i] = s_sigma_0[:self.n_slice]
                    s_mu[j][i] = s_mu_0
                if name_result == y:
                    count += 1
                if self.is_print :
#                     print(s_print_r)
                    self.log_process(msg=s_print_r)
                hc = -dq_c + dq_r
                lc_pow = -self.xi * hc
                lc = 1 / (1 + m.pow(m.e, lc_pow.real))
                hc_x[j] = lc
                l1 = l1 + lc.real + self.alpha * dq_c
                if self.is_print:
                    msg = f"{j}; dq_c: {round(dq_c, 2)}; dq_r: {round(dq_r, 2)}; lc: {round(lc, 2)};" \
                          f" hc: {round(hc, 2)}; l1: {round(l1, 2)} "
                    msg = msg + f"\nc={y} i:{ind_c[j]} r={name_result} i:{ind_r}"
                    msg = msg + f"\nc : s_tau={s_tau[j][my_i]} s_sigma={s_sigma[j][my_i][:6]} s_mu={s_mu[j][my_i][:6]}"
                    msg = msg + f"\nr: s_tau={s_tau[j][ind_r]} s_sigma={s_sigma[j][ind_r][:6]} s_mu={s_mu[j][ind_r][:6]}"
                    # print(msg)
                    self.log_process(msg=msg)
                accuracy = count / p_x_te * 100
                s_a = "a = " + str(round(accuracy, 2)) + "% " + "L1=" + str(round(l1 / p_x_te, 2))
                if not self.is_print:
                    pb2.print(j, s_a)
                # TODO: uncomment after check
            print('\010' * (len(p_1) + 2), end="")
            l1 = l1 / p_x_te
            end2 = t.time()
            proses = end2 - start2
            title = "DLQDF_13-MQDF2_7"
            msg = f"{title} It= {it} a= {round(accuracy, 4)} %; L1= {round(l1, 4)}; t= {round(proses, 2)} s"
            # print(msg)
            self.log_process(msg=msg)
            is_dlqdf2 = True if is_dlqdf is False else False
            is_dlqdf = True
            it += 1
            ind_r_f = ind_r
        return name_result

    def update_sgd_01(self, is_genuine_class, hc_x_j, sigma_t, tau_t, mu_t, sigma_t0, tau_t0, mu_t0):
        sigma_t1 = np.zeros(self.n_slice)
        tau_t1 = np.zeros(1)
        mu_t1 = np.zeros(self.p_vec)
        if is_genuine_class:
            # genuine class
            sigma_t1 = sigma_t - self.lr1 * ((self.xi * hc_x_j * (1 - hc_x_j) * sigma_t0) + self.alpha * sigma_t0)
            tau_t1 = tau_t - self.lr2 * ((self.xi * hc_x_j * (1 - hc_x_j) * tau_t0) + self.alpha * tau_t0)
            mu_t1 = mu_t - self.lr3 * ((self.xi * hc_x_j * (1 - hc_x_j) * mu_t0) + self.alpha * mu_t0)
#             print(f"check mev {tau_t1} = {tau_t} - {self.lr2} * (({self.xi} * {hc_x_j} * (1 - {hc_x_j}) * {tau_t0}) + {self.alpha} * {tau_t0})")
        else:
            # rival class and other else
            sigma_t1 = sigma_t - self.lr1 * (self.xi * hc_x_j * (1 - hc_x_j) * sigma_t0)
            tau_t1 = tau_t - self.lr2 * (self.xi * hc_x_j * (1 - hc_x_j) * tau_t0)
            mu_t1 = mu_t - self.lr3 * (-1 * self.xi * hc_x_j * (1 - hc_x_j) * mu_t0)
        # mu_t1 = mu_t
        return sigma_t1, tau_t1, mu_t1

    # 5 - 16 Discriminative Learning Quadratic Discriminant Function for Handwriting Recognition -- liu2004
    # i = idx class w, j = idx testing, k = bound.
    def mqdf2_7_00(self, x, w, is_dlqdf=False):
        s_print = ""
        x_s_m = x - w.mean
        norm = np.linalg.norm(x_s_m)
        r1 = norm ** 2
        k = w.k
        delta = np.abs(w.minor_eig_val())
        # k = w.k if self.all_k is None else self.all_k
        delta = np.abs(w.eig_val_min)
        eig_val = np.abs(w.eig_val[:self.n_slice])
        # eig_val = np.abs(w.eig_val)
        lambda_ij = np.power(eig_val, -1.0) if is_dlqdf else np.power(eig_val, 1.0)
        # delta = np.power(w.eig_val_min, 1) if w.eig_val_min > 1 else np.array([1]) # make error
        # one_per = np.where(eig_val > 1, np.power(eig_val, -1.0), np.power(1.0, 1.0))
        # eig_vec = w.eig_vec[:self.n_slice].real
        eig_vec = w.eig_vec.real
        len_ij = x_s_m.shape
        x_s_m = x_s_m.reshape(len_ij[0], 1)
        p_ij = np.dot(eig_vec, x_s_m)
        p2_ij = np.power(p_ij, 2.0)
        sum_1_3 = np.dot(np.transpose(lambda_ij), p2_ij)
        if isinstance(sum_1_3, np.ndarray):
            slice_sum_1_3 = sum_1_3[0:k]
        else:
            slice_sum_1_3 = sum_1_3
        sum_1 = np.sum(slice_sum_1_3)
        slice_sum_2_2 = p2_ij[0:k]
        sum_2_2 = np.sum(slice_sum_2_2)
        sum_2 = 1 / delta * (r1 - sum_2_2)
        try:
            sum_3_1 = np.log(eig_val)
        except RuntimeWarning:
            print(f"{w.name} - {x[0:5]}")
        slice_sum_3_1 = np.abs(sum_3_1[0:k])
        sum_3 = np.sum(slice_sum_3_1)
        # my_dk = self.n_slice if self.n_slice < k else self.n_slice - k
        my_d_k = (self.n_slice - k)
        # delta = w.minor_eig_val() if is_dlqdf else delta
        sum_4 = my_d_k * m.log(delta)
        sum_2 = sum_2[0] if isinstance(sum_2, np.ndarray) else sum_2
        sum_4 = sum_4[0] if isinstance(sum_4, np.ndarray) else sum_4
        g2_7 = sum_1 + sum_2 + sum_3 + sum_4
        # partial derivative
        s_sigma_1 = np.sign(-m.e) * (np.abs(m.e) ** (-lambda_ij)) * p2_ij
        s_sigma_0 = s_sigma_1.diagonal() + 1
        s_tau_1 = np.sign(-m.e) * (np.abs(m.e) ** (-delta)) * (r1 - sum_2_2) # prevent with real number
        s_tau_0 = s_tau_1 + my_d_k
        s_mu_1_1 = delta - lambda_ij[:k]
        my_reshape = self.n_slice if self.n_slice < k else k
        s_mu_1_2 = s_mu_1_1.reshape(my_reshape, 1).dot(p_ij[:k].reshape(1, my_reshape)).diagonal()
        s_mu_1_3 = s_mu_1_2.reshape(1, my_reshape).dot(eig_vec[:k]).reshape(self.p_vec, 1)
        s_mu_2 = 2 * delta * x_s_m
        s_mu_0 = 2 * s_mu_1_3 - s_mu_2
        s_mu_0 = s_mu_0.reshape(self.p_vec)
#         delta = delta[0] if isinstance(delta, np.ndarray) else delta
        try:
#             s_print = w.name + "; " + str(round(g2_7, 2)) + "= " + str(round(sum_1, 2)) + \
#                       "+" + str(round(sum_2, 2)) + "+" + str(round(sum_3, 2)) + "+" + str(round(sum_4, 2)) \
#                       + " mev:" + str(round(delta))+"; r1 = "+str(round(r1,2)) 
            s_print = f"{w.name}; {round(g2_7, 2)} = {round(sum_1, 2)} + {round(sum_2, 2)} + {round(sum_3, 2)} + {round(sum_4, 2)}"
            s_print = s_print + f" mev: {round(delta,4)} ri: {round(r1,2)} pij: {sum_2_2} d-k: {my_d_k} "
        except Exception as e:
            msg = f"{e} = {self.j} ; {w.name}"
            print(msg)
            self.log_process(msg=msg)
        return g2_7, s_tau_0, s_sigma_0, s_mu_0, s_print

    def mqdf2_7_01(self, x, w, is_dlqdf=False):
        s_print = ""
        x_s_m = x - w.mean
        norm = np.linalg.norm(x_s_m)
        # r1 = norm ** 2
        k = w.k
        # k = w.k if self.all_k is None else self.all_k
        delta = np.abs(w.eig_val_min)
        # eig_val = np.abs(w.eig_val[:self.n_slice])
        eig_val = w.eig_val
        lambda_ij = np.power(eig_val, -1.0) if is_dlqdf else np.power(eig_val, -1.0)
        # delta = np.power(w.eig_val_min, 1) if w.eig_val_min > 1 else np.array([1]) # make error
        # one_per = np.where(eig_val > 1, np.power(eig_val, -1.0), np.power(1.0, 1.0))
        # eig_vec = w.eig_vec[:self.n_slice].real
        eig_vec = w.eig_vec.real
        len_ij = x_s_m.shape
        x_s_m = x_s_m.reshape(len_ij[0], 1)
        p_ij = np.dot(np.transpose(eig_vec), x_s_m)
        p2_ij = np.power(p_ij, 2.0)
        lambda_ij_1 = lambda_ij[0:k]
        p2_ij_1 = p2_ij[0:k]
        sum_1_3 = np.dot(np.transpose(lambda_ij_1), p2_ij_1)
        if isinstance(sum_1_3, np.ndarray):
            slice_sum_1_3 = sum_1_3[0:k]
        else:
            slice_sum_1_3 = sum_1_3
        sum_1 = np.sum(slice_sum_1_3)
        slice_sum_2_2 = p2_ij[0:k]
        sum_2_2 = np.sum(slice_sum_2_2)
        sum_2_1 = np.sum(p2_ij)
        sum_2 = 1 / delta * (sum_2_1 - sum_2_2)
        sum_3_1 = np.log10(eig_val)
        slice_sum_3_1 = np.abs(sum_3_1[0:k])
        sum_3 = np.sum(slice_sum_3_1)
        # my_dk = self.n_slice if self.n_slice < k else self.n_slice - k
        my_d_k = (self.n_slice - k)
        delta = w.minor_eig_val() if is_dlqdf else delta
        sum_4 = my_d_k * m.log(delta)
        sum_2 = sum_2[0] if isinstance(sum_2, np.ndarray) else sum_2
        sum_4 = sum_4[0] if isinstance(sum_4, np.ndarray) else sum_4
        g2_7 = sum_1 + sum_2 + sum_3 + sum_4
        # partial derivative
        s_sigma_1 = np.sign(-m.e) * (np.abs(m.e) ** (-lambda_ij)) * p2_ij
        s_sigma_0 = s_sigma_1.diagonal() + 1
        s_tau_1 = np.sign(-m.e) * (np.abs(m.e) ** (-delta)) * (sum_2_1 - sum_2_2)
        s_tau_0 = s_tau_1 + my_d_k
        s_mu_1_1 = delta - lambda_ij[:k]
        my_reshape = self.n_slice if self.n_slice < k else k
        s_mu_1_2 = s_mu_1_1.reshape(my_reshape, 1).dot(p_ij[:k].reshape(1, my_reshape)).diagonal()
        s_mu_1_3 = s_mu_1_2.reshape(1, my_reshape).dot(eig_vec[:k]).reshape(self.p_vec, 1)
        s_mu_2 = 2 * delta * x_s_m
        s_mu_0 = 2 * s_mu_1_3 - s_mu_2
        s_mu_0 = s_mu_0.reshape(self.p_vec)
        delta = delta[0] if isinstance(delta, np.ndarray) else delta
        try:
            s_print = w.name + "; " + str(round(g2_7, 2)) + "= " + str(round(sum_1, 2)) + \
                      "+" + str(round(sum_2, 2)) + "+" + str(round(sum_3, 2)) + "+" + str(round(sum_4, 2)) \
                      + " mev:" + str(round(delta))
        except Exception as e:
            msg = f"{e} = {self.j} ; {w.name}"
            print(msg)
            self.log_process(msg=msg)
        return g2_7, s_tau_0, s_sigma_0, s_mu_0, s_print
