import pandas as pd
import numpy as np


class SelectColFromFeatureSelection(object):
    def __init__(self, x, y, xy, d, x_te):
        self.x = x
        self.y = y
        self.xy = xy
        self.d = d
        self.x_te = x_te

    def get_new_xy(self):
        # pass
        l_name_col = list(self.xy.get_xy_sel().columns)
        name_col = []
        new_te = []
        for i in range(self.d):
            sel_col = self.x[:, i]
            for j in range(self.xy.get_d()):
                val_col = np.transpose(self.xy.get_xy_sel().iloc[:, j].values)
                compare = sel_col == val_col
                if compare.all():
                    # data_.append(sel_col)
                    name_col.append(l_name_col[j])
                    if self.x_te is not None:
                        new_te.append(self.x_te[:, j])
                    break
        new_x = self.x.reshape(len(self.x), self.d)
        # new_xy = np.concatenate((new_x, self.y), axis=1)
        # name_col.append('label')
        self.xy = pd.DataFrame(new_x, columns=name_col)
        df_y = pd.DataFrame(self.y, columns=['label'])
        self.xy['label'] = df_y['label']
        self.x = new_x
        self.x_te = np.transpose(new_te)

    def get_xy_sel(self):
        return self.xy

    def get_d(self):
        return self.d

    def get_x(self):
        return self.x

    def get_y(self):
        return self.y

    def get_te_x(self):
        return self.x_te
