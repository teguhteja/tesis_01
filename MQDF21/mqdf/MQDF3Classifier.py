import mqdf
import mqdf.W as W
import numpy as np
import mqdf.MqdfBasic as mb
import time as t
import math as m
from sklearn.metrics import confusion_matrix


class MQDF3Classifier:

    def __init__(self, class_w, group_name, n_slice, d, log_process):
        self.class_w = class_w
        self.group_name = group_name
        if n_slice != 0 and n_slice is not None:
            self.n_slice = n_slice
        else:
            self.n_slice = d
        self.all_k = None
        self.d = 0
        self.all_delta = None
        self.name_result = None
        self.conf_matrix = None
        self.is_print = False
        self.log_process = log_process

    def set_all_k(self, k):
        self.all_k = k

    def set_is_print(self, val):
        self.is_print = val

    def set_all_delta(self, delta):
        self.all_delta = delta

    def set_cic_delta(self, cic):
        if cic is not None:
            for w_ in self.class_w:
                name_w = w_.name
                result = np.argwhere(cic == name_w)
                idx = result[0][0]
                mev = cic[idx][1]
                w_.eig_val_min = mev
    #TODO : create new mqdf2
    def mqdf2_classifier(self, x_te, y_te, is_mqdf3=False):
        p_class_w = len(self.class_w)
        p_x_te = len(x_te)
        y_pred = [None]*p_x_te
        start2 = t.time()
        count = 0
        pb2 = mqdf.ProgressBar(p_x_te)
        accuracy = 0.0
        s_a = ""
        name_result = None
        for j in range(p_x_te):
            x = x_te[j]
            y = y_te[j][0]
            name_result = ""
            acc_temp = float("inf")
            for i in range(p_class_w):
                w = self.class_w[i]
                # if self.all_k is not None and w.k is not None:
                #     w.k = self.all_k
                #     w.minor_eig_val()
                # g2_7 = self.mqdf3_11_c(x, w)
                w.set_n_slice(self.n_slice)
                w.k = self.all_k if self.all_k is None and self.all_k == 0 else w.k
                w.eig_val_min = w.eig_val_min if self.all_delta is None else self.all_delta
                g2_7, s_print = self.mqdf3_11_c(x, w) if is_mqdf3 else self.mqdf2_7_c(x, w, j)
                # g2_7 = self.mqdf3_11_c(x, w) if is_mqdf3 else self.dlqdf2_25_c(x, w)
                if g2_7 < acc_temp:
                    name_result = w.name
                    acc_temp = g2_7
                    s_r = s_print
                if w.name == y:
                    s_g = s_print
            s_b = ""
            if name_result == y:
                count += 1
                s_b = f"{j} OK "
            else:
                s_b = f"{j} NOT {s_r}  X {s_g}"
            if self.is_print:
                print(s_b)
            # else:
            #     if self.is_print:
            #         # print(s_g)
            #         # print(s_r)
            #         self.log_process(msg=s_g)
            #         self.log_process(msg=s_r)
            if not self.is_print:
                pb2.print(j, s_value=s_a)
            y_pred[j] = name_result
            accuracy = count / p_x_te * 100
            s_a = "a=" + str(round(accuracy, 2)) + "%"
        end2 = t.time()
        proses = end2 - start2
        title = "MQDF3_11" if is_mqdf3 else "MQDF2_7 in MQDF3"
        msg = f"{title} a= {round(accuracy, 4)} %; time= {round(proses, 2)} k=, {self.all_k} delta= {self.all_delta} slice= {self.n_slice}"
        # print(msg)
        self.log_process(msg=msg)
        y_true = y_te[:, 0]
        self.conf_matrix = confusion_matrix(y_true, y_pred, labels=self.group_name)

    def get_conf_matrix(self):
        return self.conf_matrix

    def get_name_result(self):
        return self.name_result

    # j = idx testing,
    def mqdf2_7_c(self, x, w, j=0):
        x_s_m = x - w.mean
        norm = np.linalg.norm(x_s_m)
        r1 = norm ** 2
        k = w.k
        delta = np.abs(w.eig_val_min)
        eig_val_d = w.eig_val[:self.n_slice]
        eig_val_k = eig_val_d[:k]
        lambda_ij = np.power(eig_val_k, -1.0)
        eig_vec_d = w.eig_vec[:self.n_slice].real
        eig_vec_k = eig_vec_d[:k]
        len_ij = x_s_m.shape
        x_s_m = x_s_m.reshape(len_ij[0], 1)
        p_ij = np.dot(eig_vec_k, x_s_m)
        p2_ij = np.power(p_ij, 2.0)
        sum_1_3 = np.dot(np.transpose(lambda_ij), p2_ij)
        slice_sum_1_3 = sum_1_3[0:k] if isinstance(sum_1_3, np.ndarray) else sum_1_3
        sum_1 = np.sum(slice_sum_1_3)
        slice_sum_2_2 = p2_ij[0:k]
        sum_2_2 = np.sum(slice_sum_2_2)
        sum_2 = 1 / delta * (r1 - sum_2_2)
        sum_3_1 = np.log(eig_val_k)
        slice_sum_3_1 = (sum_3_1[0:k])
        sum_3 = np.sum(slice_sum_3_1)
        try:
            sum_4 = (w.d - k) * m.log(delta)
        except:
            msg = f"{w.name} {j} : {delta}"
            # print(msg)
            self.log_process(msg=msg)
        g2_7 = sum_1 + sum_2 + sum_3 + sum_4  # no use sum_4 because to high minus
        if self.is_print:
            print("i: ", j, "; gn:", w.name, "; ", round(g2_7, 2), "= ", round(sum_1, 2), "+ ", round(sum_2, 2), "+ ",
              round(sum_3, 2), "+ ", round(sum_4, 2))
        s_print = w.name + ":" + str(round(g2_7, 2)) + "= " + str(round(sum_1, 2)) + "+ " + str(round(sum_2, 2)) + "+ " + \
                  str(round(sum_3, 2)) + "+ " + str(round(sum_4, 2))
        return g2_7, s_print

    def mqdf2_7_1(self, x, w, j=0):
        x_s_m = x - w.mean
        norm = np.linalg.norm(x_s_m)
        # r1 = norm ** 2
        k = w.k
        delta = w.eig_val_min
        eig_val = w.eig_val
        lambda_ij = np.power(eig_val, -1.0)
        eig_vec = w.eig_vec.real
        len_ij = x_s_m.shape
        x_s_m = x_s_m.reshape(len_ij[0], 1)
        p_ij = np.dot(np.transpose(eig_vec), x_s_m)
        p2_ij = np.power(p_ij, 2.0)
        lambda_ij_1 = lambda_ij[0:k]
        p2_ij_1 = p2_ij[0:k]
        sum_1_3 = np.dot(np.transpose(lambda_ij_1), p2_ij_1)
        if isinstance(sum_1_3, np.ndarray):
            slice_sum_1_3 = sum_1_3[0:k]
        else:
            slice_sum_1_3 = sum_1_3
        sum_1 = np.sum(slice_sum_1_3)
        slice_sum_2_2 = p2_ij[0:k]
        sum_2_2 = np.sum(slice_sum_2_2)
        sum_2_1 = np.sum(p2_ij)
        sum_2 = 1 / delta * (sum_2_1 - sum_2_2)
        sum_3_1 = np.log(eig_val)
        slice_sum_3_1 = (sum_3_1[0:k])
        sum_3 = np.sum(slice_sum_3_1)
        sum_4 = (self.n_slice - k) * m.log(delta.real)
        g2_7 = sum_1 + sum_2 + sum_3 + sum_4   # no use sum_4 because to high minus
        print_s = f"i: {j} ; gn: {w.name}  {round(g2_7, 2)} =  {round(sum_1, 2)} + {round(sum_2, 2)} + {round(sum_3, 2)} + {round(sum_4, 2)}"
        # if self.is_print:
        #     print(print_s)
        return g2_7, print_s

    def mqdf3_11_c(self, x, w, j=0):
        x_s_m = x - w.mean
        norm = np.linalg.norm(x_s_m)
        r1 = norm ** 2
        k = w.k
        delta = np.abs(w.new_eig_val_min)
        eig_val = w.eig_val_int[:self.n_slice]
        one_per = np.power(eig_val[:k], -1.0)
        eig_vec_d = w.eig_vec_int[:self.n_slice].real
        eig_vec_k = eig_vec_d[:k]
        sum_1_1 = np.dot(eig_vec_k, x_s_m)
        sum_1_2 = np.power(sum_1_1, 2.0)
        sum_1_3 = np.dot(np.transpose(one_per), sum_1_2)
        if isinstance(sum_1_3, np.ndarray):
            slice_sum_1_3 = sum_1_3[0:k]
        else:
            slice_sum_1_3 = sum_1_3
        sum_1 = np.sum(slice_sum_1_3)
        slice_sum_2_2 = sum_1_2[0:k]
        sum_2_2 = np.sum(slice_sum_2_2)
        sum_2 = 1 / delta * (r1 - sum_2_2)
        sum_3_1 = np.log(eig_val)
        slice_sum_3_1 = sum_3_1[0:k]
        sum_3 = np.sum(slice_sum_3_1)
        sum_4 = (w.d - k) * m.log(delta)
        g2_7 = sum_1 + sum_2 + sum_3 + sum_4
        s_print = w.name + ":" + str(round(g2_7, 2)) + "= " + str(round(sum_1, 2)) + "+ " + str(
            round(sum_2, 2)) + "+ " + \
                  str(round(sum_3, 2)) + "+ " + str(round(sum_4, 2))
        return g2_7, s_print
