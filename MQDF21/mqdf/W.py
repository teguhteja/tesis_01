import numpy as np


class W:

    def __init__(self, name, mean, eig_val, eig_vec, d):
        self.name = name
        self.mean = mean
        self.eig_val = eig_val
        self.eig_vec = eig_vec
        # self.d = len(eig_val)
        self.d = d
        self.eig_val_int = None
        self.eig_vec_int = None
        self.eig_val_min = None
        self.new_eig_val_min = None
        self.k = None
        self.n_slice = None

    def set_inter_eig(self, eig_val_int, eig_vec_int):
        self.eig_val_int = eig_val_int
        self.eig_vec_int = eig_vec_int

    def set_n_slice(self, n_slice):
        self.n_slice = n_slice

    def minor_eig_val(self, k=None):
        my_d = 0
        # print(self.name,":",self.d,":",k,":",self.k)
        my_d = self.n_slice if self.n_slice is not None else self.d
        my_k = self.k if self.k is not None else k
        p_slice = my_d if my_d - my_k == 0 else my_d - my_k
        st = my_k + 1  # should only k
        en = my_k + p_slice
        eig_val_slice = self.eig_val[st:en]
        sum_slice = np.sum(eig_val_slice)
        eig_val_min = (1 / p_slice) * sum_slice
        eig_val_min = 1 if eig_val_min == 0 else eig_val_min
        return eig_val_min

