import numpy as np
import pandas as pd

class SmallTesting:
    def __init__(self, pd_xy_tr, d, my_group_name_tr, ratio, is_random, fixed_n, _x, _y):
        self.pd_xy_tr = pd_xy_tr
        self.d = d
        self.ratio = ratio
        self.gr_nm = my_group_name_tr
        self.is_random = is_random
        self.fixed_n = fixed_n
        self._x = None
        self._y = None
        self.x_te_inc = []
        self.y_te_inc = []

    def process_small_testing(self):
        is_ratio = True if self.ratio != 0.0 else False
        is_fixed = True if self.fixed_n != 0 else False
        total = 0
        if self._x is None and self._y is None:
            x_te, y_te = self.convert_pxy_to_npxy()
        else:
            x_te = self._x
            y_te = self._y
        for name in self.gr_nm:
            idx_range, temp = np.where(y_te == name)
            if is_ratio:
                n_smp = int(self.ratio*len(idx_range))
            if is_fixed:
                n_smp = self.fixed_n
            total = total + n_smp
            if self.is_random:
                indexes = np.random.choice(idx_range, size=n_smp, replace=False)
                if len(self.x_te_inc) == 0:
                    self.x_te_inc = x_te[indexes]
                    self.y_te_inc = y_te[indexes]
                else:
                    self.x_te_inc = np.concatenate((self.x_te_inc,x_te[indexes]), axis=0)
                    self.y_te_inc = np.concatenate((self.y_te_inc,y_te[indexes]), axis=0)
            else:
                take_idx = idx_range[:n_smp]
                if len(self.x_te_inc) == 0:
                    self.x_te_inc = x_te[take_idx]
                    self.y_te_inc = y_te[take_idx]
                else:
                    self.x_te_inc = np.concatenate((self.x_te_inc,x_te[take_idx]), axis=0)
                    self.y_te_inc = np.concatenate((self.y_te_inc,y_te[take_idx]), axis=0)
        self.x_te_inc = self.x_te_inc.astype(np.float)
        print("Small Testing :", total, "ratio ", self.ratio, "Fixed N:", self.fixed_n, "random status :",
              self.is_random)

    def convert_pxy_to_npxy(self):
        np_xy_tr = self.pd_xy_tr.to_numpy()
        tr_x, tr_y = np_xy_tr[:, :self.d], np_xy_tr[:, -1]
        len_x = tr_y.shape
        tr_y = tr_y.reshape(len_x[0], 1)
        return tr_x, tr_y

    def get_xy(self):
        return self.x_te_inc, self.y_te_inc;