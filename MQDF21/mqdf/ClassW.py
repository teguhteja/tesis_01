import mqdf
import mqdf.MqdfBasic as mb
import mqdf.W as w
import numpy as np
import math as m
from mqdf.ProgressBar import ProgressBar as my_pb


class ClassW:

    def __init__(self, my_xy, gr_nm, my_log, is_print=False):
        self.xy = my_xy
        self.my_class_w = []
        self.group_name = gr_nm
        self.gamma = None
        self.all_k = None
        self.var = None
        self.n_slice_eig_vec = 0
        self.n_round = 0
        self.d = 0
        self.min_eig_val_i = None
        self.my_log = my_log
        self.is_print = is_print

    def set_gamma(self, gamma):
        self.gamma = gamma

    def set_d(self, d):
        self.d = d

    def set_minor_eig_val_all(self, min_eig_val):
        self.min_eig_val_i = min_eig_val

    def set_all_k(self, k):
        self.all_k = k

    def set_n_round(self, n_round):
        self.n_round = n_round

    def set_n_slice_eig_vec(self, n):
        self.n_slice_eig_vec = n

    def deplet_xy(self, is_gso=False, is_mqdf3=False):
        my_mb = mb()
        my_var = 0.0
        # percent_print = len(self.group_name) / 10
        j = 0
        # k = 0
        pb = my_pb(len(self.group_name))
        for name in self.group_name:
            # if j < 66:
            xy_select_nm = self.xy[self.xy.label == name]
            del xy_select_nm['label']
            np_x = xy_select_nm.to_numpy()
            np_x = np_x.astype(np.float)
            mean = my_mb.mean_class(np_x)
            my_var = my_var + np.var(np_x)
            cov_matrix_temp = my_mb.cov_matrix(np_x)
            eig_val, eig_vec = my_mb.calc_eigen_val_vec(cov_matrix_temp)
            p_vec = len(eig_val)
            n_d = self.d
            if self.all_k is None:
                k = self.principle_axes(eig_vec)
            else:
                k = self.all_k
            if self.n_slice_eig_vec != 0 and self.n_slice_eig_vec is not None :
                # eig_val, eig_vec = eig_val[:self.n_slice_eig_vec], eig_vec[:self.n_slice_eig_vec]
                eig_vec = eig_vec[:self.n_slice_eig_vec]
                n_d = self.n_slice_eig_vec
            if self.n_round != 0:
                eig_val = np.round(eig_val, self.n_round)
                eig_vec = np.round(eig_vec, self.n_round)
            eig_vec = self.gram_schmidt(eig_vec) if is_gso else eig_vec
            # w_ = w.W(name, mean, np.absolute(eig_val), eig_vec, n_d)
            w_ = w.W(name, mean, eig_val, eig_vec, n_d)
            w_.k = k
            w_.eig_val_min = self.minor_eig_val(k, eig_val) if self.min_eig_val_i is None else self.min_eig_val_i
            # w_.eig_val_min = self.minor_eig_val(k, np.absolute(eig_val)) if self.min_eig_val_i is None else self.min_eig_val_i
            # this function for mqdf3
            # if is_mqdf3:
            #     inter_pol_cov_matrix = self.proses_interpolation_cov(p_vec, cov_matrix_temp)
            #     eig_val_inter, eig_vec_inter = my_mb.calc_eigen_val_vec(inter_pol_cov_matrix)
            #     w_.set_inter_eig(eig_val_inter, eig_vec_inter)
            #     w_.new_eig_val_min = self.avg_complement_subspace(0, np.absolute(eig_val))
            # if is_gso is False:
            #     w_.k = self.principle_axes(eig_vec) # made error because complex number. but now k is set
            # w_.k = self.all_k if self.all_k is None else w_.principle_axes()
            self.my_class_w.append(w_)
            s_print = w_.name +" DONE"
#             pb.print(j, s_print)
            # TODO: uncomment after check
            j += 1
            if self.is_print:
                print(f'\nLabel {name}. mean: {mean} d:{n_d} mev:{w_.eig_val_min} \n'
                      f'eig_val : {eig_val} \n'
                      f'eig_vec : {eig_vec} \n')
        self.var = my_var / len(self.group_name)
        msg = f"groups= {len(self.group_name)} gamma= {self.gamma} k= {self.all_k} n_eig_vec= {self.n_slice_eig_vec} n_round= {self.n_round}"
        # print(msg)
        self.my_log(msg=msg)

    def gram_schmidt(self, A):
        """Orthogonalize a set of vectors stored as the columns of matrix A.
        https://gist.github.com/iizukak/1287876/edad3c337844fac34f7e56ec09f9cb27d4907cc7#gistcomment-2935521
        """
        # Get the number of vectors.
        n = A.shape[1]
        for j in range(n):
            # To orthogonalize the vector in column j with respect to the
            # previous vectors, subtract from it its projection onto
            # each of the previous vectors.
            for k in range(j):
                A[:, j] -= np.dot(A[:, k], A[:, j]) * A[:, k]
            A[:, j] = A[:, j] / np.linalg.norm(A[:, j])
        return A

    def get_class_w(self):
        return self.my_class_w

    def get_group_name(self):
        return self.group_name

    def proses_interpolation_cov(self, d, cov):
        sigma = np.trace(cov) / d
        i = np.identity(d)
        new_cov = (1 - self.gamma) * cov + self.gamma * sigma * i
        return new_cov

    def get_avr_var(self):
        return self.var

    def normalization_vector(self, eig_vec):
        return eig_vec / np.linalg.norm(eig_vec)

    def minor_eig_val(self, k, eig_val):
        # if self.n_slice_eig_vec != 0 and self.n_slice_eig_vec is not None:
        #     p_slice = self.n_slice_eig_vec - k
        # else:
        p_slice = self.d - k
        st = k + 1  # should only k
        en = k + p_slice
        eig_val_slice = eig_val[st:en]
        sum_slice = np.sum(eig_val_slice)
        eig_val_min = (1 / p_slice) * sum_slice
        # eig_val_min = np.round(eig_val_min, 14)
        # eig_val_min = 1 if eig_val_min == 0.0 else eig_val_min
        # eig_val_min += 1
        return eig_val_min

    def avg_complement_subspace(self, k, eig_val):
        if k == 0:
            k = self.all_k
        eig_val_slice = eig_val[0:k]
        new_eig_val_min = np.average(eig_val_slice)
        return new_eig_val_min

    def principle_axes(self, eig_vec):
        r_eig_vec = eig_vec.real
        uni_0 = r_eig_vec.T.dot(r_eig_vec)
        uni_1 = np.round(uni_0.real, 14)
        uni_2 = np.sum(uni_1, axis=1, dtype=np.float32)
        k = np.count_nonzero((uni_2 == 1.0))
        return k
