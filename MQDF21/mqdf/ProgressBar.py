import time as t
import math as m


class ProgressBar:
    def __init__(self, task, max_bar=20):
        self.task = task
        self.max_bar = max_bar
        self.task_percent = m.ceil(task / self.max_bar)
        self.is_start = False
        self.is_deca = self.task_percent * self.max_bar == task
        self.s_print = ""
        self.s_etd = ""
        self.j0 = -1
        self.is_gen_etd = False
        self.t_start = None
        self.s_value = ""

    def print(self, seq, s_value=""):
        self.s_value = s_value
        milestone_deca = (seq+1) % self.task_percent
        p_bar = m.ceil(seq / self.task_percent)
        n_percent = round((seq / (self.task/100)),2)
        self.gen_etd(seq)
        if milestone_deca == 0:
            self.print_pb(p_bar, n_percent)
            self.is_start = True
        if self.is_start:
            self.print_pb(p_bar, n_percent)
        if self.task == (seq + 1) and self.is_deca is False:
            self.print_pb(self.max_bar, n_percent)
            self.last_delete()
        if self.task == (seq + 1) and self.is_deca is True:
            self.last_delete()

    def print_pb(self, p_bar, n_percent):
        n_backspace = len(self.s_print)
        # TODO: uncomment after check
        if self.is_start:
            print('\010' * n_backspace, end="")
        s_temp = "["
        s_temp = s_temp + "=" * (p_bar)
        s_temp = s_temp + " " * (self.max_bar - p_bar)
        s_temp = s_temp + "] " + str(n_percent) + "%"
        s_temp = s_temp + " " + self.s_etd
        s_temp = s_temp + " "+self.s_value
        self.s_print = s_temp
        print(s_temp, end="", sep="")
        # TODO: uncomment after check

    def last_delete(self):
        n_backspace = len(self.s_print)
        t.sleep(1)
        print('\010' * n_backspace, end="")
        # TODO: uncomment after check
        self.is_start = False

    def gen_etd(self, seq):
        if self.j0 < seq and self.is_gen_etd is False:
            self.is_gen_etd = True
            self.j0 = seq
            self.t_start = t.time()
        else:
            self.is_gen_etd = False
            end = t.time()
            t_task = end - self.t_start
            r_task = self.task - seq
            t_time = round(t_task*r_task,2)
            self.s_etd = "ETD: " + str(t_time)+"s"
        if self.j0 - seq > 1 :
            self.j0 = -1


# print("MQDF:", end="")
# # n = 1000
# n = 3673
# Pb = ProgressBar(n, max_bar=50)
# for j in range(n):
#     t.sleep(0.001)
#     Pb.print(j)


