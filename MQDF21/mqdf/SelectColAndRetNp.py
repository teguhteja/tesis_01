import pandas as pd
import numpy as np


# select Column from csv and return as Numpy
# from main_dlqdf_01 import home
home = "/home/teguhteja/P/PythonProject"

class SelectColAndRetNp:

    def __init__(self, x, y, st_idx, en_idx, log_process):
        self.x = x
        self.y = y
        self.st_idx = st_idx
        self.en_idx = en_idx
        self.xy_sel = []
        self.d = en_idx-st_idx
        self.group_name_scarn = []
        self.log_process = log_process

    def process(self):
        f = open(home+"/Tesis/MQDF21/data/header_03.txt", "r")
        f1 = f.readlines()
        header = f1[0].split(",")
        header.append("label")
        f.close()
        names_label = ['label']

        xy = pd.read_csv(self.x, delimiter=',', header=None, names=header, dtype={"label": object})
        y = pd.read_csv(self.y, delimiter=';', header=None, names=names_label, keep_default_na=False)
        self.xy_sel = xy.iloc[:, self.st_idx:self.en_idx]
        self.xy_sel['label'] = y['label']
        # self.xy_sel
        msg = f"x: {self.x} \ny: {self.y} for training. \nRows :  {len(self.xy_sel)} Col : {(self.en_idx-self.st_idx)}"
        self.log_process(msg=msg)
        x = pd.read_csv(self.x, delimiter=',', header=None, names=None, dtype={"label": object})
        x = x.iloc[:, self.st_idx:self.en_idx]
        self.x = x.to_numpy()
        y = y.to_numpy(dtype=np.str)
        self.y = np.asarray(y, dtype=np.str)

    def process_te(self):
        name_x = self.x
        x = pd.read_csv(self.x, delimiter=',', header=None, names=None, dtype={"label": object})
        x = x.iloc[:, self.st_idx:self.en_idx]
        name_y = self.y
        y = pd.read_csv(self.y, delimiter=';', header=None, names=None, keep_default_na=False)
        self.x = x.to_numpy()
        y = y.to_numpy(dtype=np.str)
        self.y = np.asarray(y, dtype=np.str)
        # print("x:", name_x, "\ny:", self.y, " for testing. \nRows : ", len(self.x),"Col :", (self.en_idx - self.st_idx))
        msg = f"x: {name_x} \ny: {name_y} for testing. \nRows : {len(self.x)} Col : {(self.en_idx - self.st_idx)}"
        self.log_process(msg=msg)

    def distinct_group_name(self):
        group_name = self.xy_sel.label.unique()
        ls_gn = list(group_name)
        ls_gn = [x for x in ls_gn if str(x) != 'nan']
        self.group_name_scarn = tuple(ls_gn)
        msg = f"Unique Label : {len(ls_gn)}"
        self.log_process(msg=msg)
        # print("Unique Label :", len(ls_gn))

    def get_xy_sel(self):
        return self.xy_sel

    def get_d(self):
        return self.d

    def get_x(self, i=0):
        if i == 0:
            return self.x
        else:
            return  self.x[0:i]

    def get_y(self, i=0):
        if i == 0:
            return self.y
        else :
            return  self.y[0:i]

    def get_group_name(self):
        return self.group_name_scarn
