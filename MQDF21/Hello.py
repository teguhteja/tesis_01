def factorial(x):
    if x == 1:
        return 1
    else:
        return x*factorial(x - 1)


my_x = input()
arr_x = my_x.split(",")
ans = ""
for xi in arr_x:
    ans = ans + str(factorial(int(xi)))
    ans = ans + ","
ans = ans[0:len(ans) - 1]
print(ans)
