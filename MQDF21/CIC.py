import main_dlqdf_01 as md01
import datetime as dtt
import pandas as pd
import numpy as np
import time as t

class CIV:
    def __init__(self):
        pass


def import_mev_csv():
    file_nm = md01.home + "/Tesis/MQDF21/data/CIC.csv"
    pd_test = pd.read_csv(file_nm, delimiter=';', header=None, names=None, keep_default_na=False)
    np_cic = pd_test.to_numpy(dtype=np.object)
    return np_cic


def train_cic(te_x, te_y, name_group):
    print("Start Train CIC :", dtt.datetime.now())
    start = t.time()
    result = np.argwhere(te_y == name_group)
    indices = result[:, 0]
    new_te_x = te_x[indices]
    new_te_y = te_y[indices]
    end1 = t.time()
    print("proses Train CIC : ", round((end1 - start), 2), "s")
    return new_te_x, new_te_y

def start_classifier(cic):
    load_dump = md01.m_load_dump("w_1984_2384_0_None_0_0.0_.pickle")
    my_class_w_tr = load_dump[0]
    my_group_name_tr = load_dump[1]
    # average_var_class = load_dump[2]
    te_x = load_dump[3]
    te_y = load_dump[4]
    k = load_dump[5]
    n_slice_eig_vec = load_dump[6]
    d = load_dump[7]
    pd_xy_tr = load_dump[8]

    min_eig_val_i = None
    name = ["1","I", "GUWUNG" ]
    b_s_te = False
    ratio_sm = 0.0
    small_testing_random = False
    f_sample = 10

    if b_s_te:
        te_x, te_y = md01.g_s_testing(pd_xy_tr, ratio_sm, my_group_name_tr, d, is_random=small_testing_random,
                                      fixed_sample=f_sample, _x=te_x, _y=te_y)
    te_x, te_y = train_cic(te_x, te_y,name)

    md01.my_mqdf_classifier(my_class_w_tr, my_group_name_tr, te_x, te_y, k, n_slice_eig_vec, d, min_eig_val_i=
    min_eig_val_i, cic=cic, is_print=True)


def main_proses():
    cic = import_mev_csv()
    start_classifier(cic)

# if __name__ == "__main__":
#     main_proses()
