import numpy as np
import time as t
import datetime as dtt
import mqdf.SelectColAndRetNp as Scarn
import mqdf.ClassW as Mcw
import mqdf.MQDF3Classifier as Mqdf3c
import mqdf.CrossValidation as CrVa
import mqdf.QDFClassifier as Mqdf2
import main_dlqdf_01 as Md01
# for test mqdf k based number of principle axes and mqdf3 with interpolation


def classifier_mqdf(my_class_w_tr, my_group_name_tr, n_slice_eig_vec, x, y, d, is_mqdf3=False):
    print("start classifier :", dtt.datetime.now())
    start = t.time()
    # mqdf3c = Mqdf3c(my_class_w_tr, my_group_name_tr, n_slice_eig_vec, d)
    # mqdf3c.all_k = 201
    # mqdf3c.mqdf2_classifier(x, y, is_mqdf3)
    mqdf2 = Mqdf2(my_class_w_tr, my_group_name_tr)
    mqdf2.mqdf_classifier(x, y, 170, mode_loop=False)
    end1 = t.time()
    print("classifier : " + str(round(end1 - start, 2)) + "s")


def main_proses():
    st_idx = 1984
    en_idx = 2384
    d = en_idx - st_idx
    ratio_cv = 1.0
    is_bootstrap = False
    k = 170
    n_round = 0
    n_slice_eig_vec = 0
    gamma = 0.1
    is_mqdf3 = False

    my_xy_tr, my_te, my_group_name_tr = Md01.process_import(st_idx, en_idx)
    pd_xy_tr = Md01.cross_validation(ratio_cv, is_bootstrap, my_xy_tr, my_te, my_group_name_tr)
    my_class_w_tr, average_var_class = Md01.preprocessed(gamma, k, pd_xy_tr, my_group_name_tr, n_round, d,
                                                    n_slice_eig_vec=n_slice_eig_vec)
    x = my_te.get_x()
    y = my_te.get_y()
    # x1 = x[:1]
    # y1 = y[:1]
    classifier_mqdf(my_class_w_tr, my_group_name_tr, n_slice_eig_vec, x, y, d, is_mqdf3)


if __name__ == "__main__":
    main_proses()
