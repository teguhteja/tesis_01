import numpy as np
import pandas as pd
import gc
import time as t
import datetime as dtt
from mqdf.MqdfBasic import log, mean_class, inv_matrix, cov_matrix, calc_eigen_val_vec, det
# add as source in project interpreter
# import pprint as pp


dict_a_tr_mean = {}
dict_r1 = {}
dict_sum = {}

# 5 - 16 Discriminative Learning Quadratic Discriminant Function for Handwriting Recognition -- liu2004
#@profile
def mqdf2_7(loc1_group_name, loc1_list_mean, loc1_list_data_testing, loc1_list_data_testing_name, loc1_list_eig_val,
            loc1_list_eig_vec, loc1_val_k, delta, loc1_accuracy_max, d):
    start2 = t.time()
    count = 0
    global dict_a_tr_mean, dict_r1, dict_sum
    loc1_dict_a_tr_mean = dict_a_tr_mean
    loc1_dict_r1 = dict_r1
    loc1_dict_sum = dict_sum
    for k in range(len(loc1_list_data_testing)):
        a_training = loc1_list_data_testing[k]
        data_testing_name = loc1_list_data_testing_name[k]
        acc_temp = float("inf")
        name_group_temp = ""
        for loc1_i in range(len(loc1_group_name)):
            a_mean = loc1_list_mean[loc1_i]
            key_d_a_tr_m = (k, loc1_i)
            if key_d_a_tr_m not in loc1_dict_a_tr_mean:
                a_tr_mean = a_training - a_mean
                loc1_dict_a_tr_mean[key_d_a_tr_m] = a_tr_mean
                euclidean_dist = np.linalg.norm(a_tr_mean)
                r1 = pow(euclidean_dist, 2)
                loc1_dict_r1[key_d_a_tr_m] = r1
            else:
                a_tr_mean = loc1_dict_a_tr_mean[key_d_a_tr_m]
                r1 = loc1_dict_r1[key_d_a_tr_m]
            eig_val = loc1_list_eig_val[loc1_i]
            eig_vec = loc1_list_eig_vec[loc1_i]
            sum_1 = 0.0
            sum_3 = 0.0
            #d = len(eig_val)
            key_sum_all = (k, loc1_i, loc1_val_k)
            if key_sum_all not in loc1_dict_sum:
                if loc1_val_k-1 > 0:
                    prev_key_f_val = (k, loc1_i, loc1_val_k-1)
                    prev_val_sum = loc1_dict_sum[prev_key_f_val]
                    sum_1 = prev_val_sum[0]
                    sum_3 = prev_val_sum[1]
                    r1 = prev_val_sum[2]
                j = loc1_val_k - 1
                e_val = eig_val[j]
                e_vec = eig_vec[:, j]
                sum_1_1 = np.dot(np.transpose(e_vec), a_tr_mean)
                sum_1 = sum_1 + 1 / e_val * pow(sum_1_1, 2)
                r1 = r1 - pow(sum_1_1, 2)
                sum_3 = sum_3 + log(e_val)
                loc1_dict_sum[key_sum_all] = (sum_1, sum_3, r1)
            else:
                val_sum = loc1_dict_sum[key_sum_all]
                sum_1 = val_sum[0]
                sum_3 = val_sum[1]
                r1 = val_sum[2]
            sum_4 = (d - loc1_val_k) * log(delta)
            g2_7 = sum_1 + 1 / delta * r1 + sum_3 + sum_4
            #list_temp = [loc1_group_name[loc1_i], g2_7]
            #g1.append(list_temp)
            if g2_7 < acc_temp :
                name_group_temp = loc1_group_name[loc1_i]
                acc_temp = g2_7
#        pp.pprint(g1)
        if name_group_temp == data_testing_name:
            count = count + 1
    accuracy = count / len(loc1_list_data_testing) * 100
    end2 = t.time()
    proses = end2 - start2
#    print("MQDF2_7 k=", loc1_val_k, ";h2=", round(delta,4), ";a=", round(accuracy,4), "%;time=", round(proses,4))
    if loc1_accuracy_max < accuracy:
        loc1_accuracy_max = accuracy
    return loc1_accuracy_max, accuracy, proses


gc.disable()
start = t.time()
print("start init ... ", dtt.datetime.now())
X = "X-training-split.csv"
f = open("header_03.txt", "r")
f1 = f.readlines()
header = f1[0].split(",")
header.append("label")
f.close()

dt_tr_sp_0 = pd.read_csv(X, delimiter=',', header=0, dtype={"label": object})
start_ind_col = 1984
end_ind_col = 2384
p_col = end_ind_col - start_ind_col
dt_tr_sp = dt_tr_sp_0.iloc[:, start_ind_col:end_ind_col]
dt_tr_sp['label'] = dt_tr_sp_0['label']
p_col=dt_tr_sp_0.shape[1]
del dt_tr_sp_0
end1 = t.time()
print("import file : " + str(end1 - start))


start = t.time()
list_group = []
list_mean = []
list_inv_cov = []
list_det_cov = []
list_cov = []
list_eigVal = []
list_eigVec = []

group_name = dt_tr_sp.label.unique()
ls_gn = list(group_name)
ls_gn = [x for x in ls_gn if str(x) != 'nan']
group_name = tuple(ls_gn)
# print(group_name)

for i in range(len(group_name)):
    name_file = group_name[i]
    # print(name_file)
    pd3 = dt_tr_sp[dt_tr_sp.label == name_file]
    del pd3['label']
    # content_file = np.loadtxt(name_file, delimiter=';')
    content_file = pd3.to_numpy()
    list_group.append(content_file)
    list_mean.append(mean_class(content_file))
    cov_matrix_temp = cov_matrix(content_file)
    try:
        inv_matrix_temp = inv_matrix(cov_matrix_temp)
        list_inv_cov.append(inv_matrix_temp)
        del inv_matrix_temp
    except np.linalg.LinAlgError as err :
        print(err, ":", name_file)
        # print(content_file)
        # print(cov_matrix_temp)
    list_det_cov.append(det(cov_matrix_temp))
    list_cov.append(cov_matrix_temp)
    eigVal, eigVec = calc_eigen_val_vec(cov_matrix_temp)
    list_eigVal.append(eigVal.real)
    list_eigVec.append(eigVec.real)
    del content_file, cov_matrix_temp, eigVal, eigVec, name_file, pd3
end1 = t.time()
print("proses mean, eigVec, eigVal : " + str(end1 - start))

list_data_testing_name = dt_tr_sp['label'].to_list()
del dt_tr_sp['label']
list_data_testing = dt_tr_sp.to_numpy()

accuracy_max = 0.0
np_list_group_name = np.asarray(group_name, dtype=np.str)
np_list_mean = np.asarray(list_mean, dtype=np.float64)
np_list_data_testing = np.asarray(list_data_testing, dtype=np.float64)
np_list_data_testing_name = np.asarray(list_data_testing_name, dtype=np.str)
np_list_eigVal = np.asarray(list_eigVal, dtype=np.float64)
np_list_eigVec = np.asarray(list_eigVec, dtype=np.float64)

del list_group, list_det_cov, list_inv_cov, list_cov, dt_tr_sp,
del group_name, list_mean, list_data_testing, list_data_testing_name, list_eigVal, list_eigVec, ls_gn

MODE = "TESTING 2"
print("start mqdf2... ", dtt.datetime.now())
start = t.time()
name_file_log = "MQDF2_" + MODE + "_" + str(dtt.datetime.now()) + ".txt"
f = open(name_file_log, "w+")
if MODE == "TRAINING":
    for val_k in range(1, 50):
        for h2 in np.arange(0.01, 1, 0.01):
            if accuracy_max != 100.0:
                get_val = mqdf2_7(np_list_group_name, np_list_mean, np_list_data_testing, np_list_data_testing_name,
                                  np_list_eigVal, np_list_eigVec, val_k, h2, accuracy_max, p_col)
                accuracy_max = get_val[0]
                f.writelines("\nMQDF2_7; k=" + str(val_k) + ";h2=" + str(round(h2,4)) + ";a=" + str(round(get_val[1],4))
                             + "%;time=" + str(round(get_val[2],4)))
elif MODE == "TESTING 1":
    val_k = 1
    h2 = 0.6
    get_val = mqdf2_7(np_list_group_name, np_list_mean, np_list_data_testing, np_list_data_testing_name, np_list_eigVal,
                        np_list_eigVec, val_k, h2, accuracy_max, p_col)
    f.writelines("\nMQDF2_7; k=" + str(val_k) + ";h2=" + str(round(h2, 4)) + ";a=" + str(round(get_val[1], 4))
                 + "%;time=" + str(round(get_val[2], 4)))
elif MODE == "TESTING 2":
    for val_k in range(1, 2):
        for h2 in np.arange(0.01, 0.1, 0.01):
            get_val = mqdf2_7(np_list_group_name, np_list_mean, np_list_data_testing, np_list_data_testing_name,
                    np_list_eigVal, np_list_eigVec, val_k, h2, accuracy_max, p_col)
            accuracy_max = get_val[0]
            f.writelines("\nMQDF2_7; k=" + str(val_k) + ";h2=" + str(round(h2, 4)) + ";a=" + str(round(get_val[1], 4))
                         + "%;time=" + str(round(get_val[2], 4)))
end1 = t.time()
print("mqdf2 finish : " + str(end1 - start))
f.close()