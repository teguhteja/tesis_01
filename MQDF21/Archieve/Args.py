import argparse


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-b", "--boolean", help="check boolean")
    args = parser.parse_args()
    if args.boolean:
        b_b = args.boolean.lower() == 'true'
        print(b_b)


# if __name__ == "__main__":
#     main()