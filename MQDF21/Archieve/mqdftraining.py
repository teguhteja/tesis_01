import pandas as pd
from sklearn.model_selection import train_test_split


class MqdfTraining:
    X_train = []
    y_train = []
    X_test = []
    y_test = []
    p_col = 0
    group_name = ()
    X = []
    y = []

    def __init__(self, x_data, y_data):
        self.X_data = x_data
        self.y_data = y_data

    # def get_train_and_test(self):
    #     return self.X_train, self.y_train, self.X_test, self.y_test
    def get_group_name(self):
        return self.group_name

    def get_p_col(self):
        return self.p_col

    def importfile(self):
        dt_tr_sp_0 = pd.read_csv(self.X_data, delimiter=',', header=0, dtype={"label": object})
        self.p_col = dt_tr_sp_0.shape[1]
        y_tr_sp_0 = pd.read_csv(self.y_data, delimiter=',', header=0, dtype={"label": object})

        group_name_0 = y_tr_sp_0.label.unique()
        ls_gn = list(group_name_0)
        ls_gn = [x for x in ls_gn if str(x) != 'nan']
        self.group_name = tuple(ls_gn)

        self.X = dt_tr_sp_0
        self.y = y_tr_sp_0

    def sub_sampling(self, train_size):
        self.X['label'] = self.y['label']
        for name in self.group_name:
            x_filter = self.X[self.X['label'] == name]
            y_filter = self.y[self.y['label'] == name]
            del x_filter['label']
            x_train_temp, x_test_temp, y_train_temp, y_test_temp = train_test_split(x_filter,y_filter,
                                                                                    train_size=train_size)
            self.X_train.append(x_train_temp)
            self.y_train.append(y_train_temp)
            self.X_test.append(x_test_temp)
            self.y_test.append(y_test_temp)
        return self.X_train, self.y_train, self.X_test, self.y_test


# mqdf_training = MqdfTraining("X-training-split-npwk.csv", "y-training-split-npwk.csv")
# mqdf_training.importfile()
# mqdf_training.sub_sampling(0.8)
# X_tr, y_tr, X_te, y_te = mqdf_training.get_train_and_test()
# for i in range(len(X_tr)):
#     print(len(y_tr[i])  ,":", len(X_tr[i]))
# for i in range(len(X_te)):
#     print(len(y_te[i]) ,":", len(X_te[i]))

    if __name__ == '__main__':
       pass