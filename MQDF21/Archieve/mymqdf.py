import mqdf
import numpy as np
import pprint as pp

class MyMqdf:
    dict_mqdf = {}

    def __init__(self, dict_mqdf, group_name, len_d):
        self.my_x_test = []
        self.my_y_test = []
        self.d = len_d
        self.group_name = group_name
        self.dict_mqdf_basic_calc = dict_mqdf

    def fit(self, x_test, y_test):
        for elem in x_test:
            len_elem = len(elem)
            for i in range(len_elem):
                x_te = elem.iloc[i]
                self.my_x_test.append(x_te)
        for elem in y_test:
            len_elem = len(elem)
            for i in range(len_elem):
                y_te = elem.iloc[i].to_string()
                self.my_y_test.append(y_te[9:])

    def predict(self, val_k, delta):
        index = 0
        sum_4 = (self.d - val_k) * mqdf.log(delta)
        sum_2_1 = 1/delta
        predictors = []
        for x_te in self.my_x_test:
            acc_temp = float("inf")
            y_temp = ""
            g1 = []
            for name_class in self.group_name:
                mean_class = self.dict_mqdf_basic_calc[(name_class, "mean")]
                key_xte_me = (index, "xte_me")
                key_xte_r1 = (index, "r1")
                if key_xte_me not in self.dict_mqdf:
                    xte_me = x_te - np.transpose(mean_class)
                    self.dict_mqdf[key_xte_me] = xte_me
                    euc_dist = mqdf.norm_euc(xte_me)
                    r1 = euc_dist**2
                    self.dict_mqdf[key_xte_r1] = r1
                else:
                    xte_me = self.dict_mqdf[key_xte_me]
                    r1 = self.dict_mqdf[key_xte_r1]
                eig_val = self.dict_mqdf_basic_calc[(name_class, "eig_val")]
                eig_vec = self.dict_mqdf_basic_calc[(name_class, "eig_vec")]
                sum_1 = 0.0
                sum_3 = 0.0
                key_sum_all = (index, name_class, val_k)
                if key_sum_all not in self.dict_mqdf:
                    if val_k - 1 > 0:
                        prev_key_f_val = (index, name_class, val_k - 1)
                        prev_val_sum = self.dict_mqdf[prev_key_f_val]
                        sum_1 = prev_val_sum[0]
                        sum_3 = prev_val_sum[1]
                        r1 = prev_val_sum[2]
                    j = val_k - 1
                    e_val = eig_val[j]
                    e_vec = eig_vec[:, j]
                    # sum_1_1 = np.dot(np.transpose(e_vec), xte_me)
                    #sum_1_2 = mqdf.np_transpose(e_vec)
                    sum_1_1 = mqdf.np_dot(e_vec, xte_me)
                    sum_1 = sum_1 + 1 / e_val * pow(sum_1_1, 2)
                    r1 = r1 - sum_1_1**2
                    sum_3 = sum_3 + mqdf.log(e_val)
                    self.dict_mqdf[key_sum_all] = (sum_1, sum_3, r1)
                else:
                    val_sum = self.dict_mqdf[key_sum_all]
                    sum_1 = val_sum[0]
                    sum_3 = val_sum[1]
                    r1 = val_sum[2]
                g2_7 = sum_1 + sum_2_1 * r1 + sum_3 + sum_4
                # list_temp = [ name_class, g2_7]
                # g1.append(list_temp)
                if g2_7 < acc_temp:
                    y_temp = name_class
                    acc_temp = g2_7
            # print(index)
            # pp.pprint(g1)
            predictors.append(y_temp)
            index += 1
        return predictors
