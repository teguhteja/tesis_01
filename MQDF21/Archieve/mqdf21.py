import numpy as np
import math as m
import pprint as pp
import operator as op

def cov_matrix(A, opt=1):
    n = len(A)
    m_iden = np.ones(n)
    a = A - np.dot(m_iden, A) / n
    v = np.dot(np.transpose(a), a) / (n - opt)
    return v


def inv_matrix(A):
    return np.linalg.inv(A)


def mean_class(A):
    return np.mean(A, axis=0)


def det(A):
    return np.linalg.det(A)


def log(N):
    return m.log10(N)


def calc_prior_probabilities(list_group):
    prior_probalities = []
    size = 0
    for group in list_group:
        c = len(group)
        prior_probalities.append(c)
        size = size + c
    for i in range(len(prior_probalities)):
        n = prior_probalities[i]
        phi = n / size
        log_phi = log(phi)
        prior = [n, phi, log_phi]
        prior_probalities[i] = prior
    return prior_probalities

def eigenval_vec(A):
    return np.linalg.eig(A)

# mqdf1 (10)
def mqdf2_10(group_name,list_mean,list_data_testing,list_data_testing_name,list_eigVal,list_eigVec,hpow2=0):

    accuracy = 0.0
    count = 0
    for k in range(len(list_data_testing)) :
        data_testing = list_data_testing[k]
        data_testing_name = list_data_testing_name[k]
        g1 = []
        for i in range(len(group_name)):
            a_training = np.array(data_testing)
            a_mean = np.array(list_mean[i])
            a_tr_mean = a_training - a_mean
            eigVal = list_eigVal[i]
            eigVec = list_eigVec[i]
            sum_1 = 0.0
            prod_2 = 1.0
            for j in range(len(eigVal)):
                eVal = eigVal[j]
                eVec = eigVec[:,j]
                sum_2 = eVal+hpow2
                sum_3 = np.dot(np.transpose(eVec),a_tr_mean)
                sum_1 = sum_1 + 1/sum_2*pow(sum_3,2)
                prod_2 = prod_2*sum_2
            g1_6 = sum_1 + log(prod_2)
            list_temp = [group_name[i],g1_6]
            g1.append(list_temp)
        #pp.pprint(g1)
        key,value = min(g1, key=lambda x: x[1] )
        #print(data_testing_name,' -- ',key, '--',value)
        if (key == data_testing_name):
            count = count + 1
    accuracy = count / len(list_data_testing) * 100
    #print("accuracy : ", accuracy, "%")
    print(hpow2,"\t", accuracy, "%")


group_name = ['Iron', 'Copper','Zinc','Nickel','Tin']

list_group = []
list_mean = []
list_inv_cov = []
list_det_cov = []
list_cov = []
list_eigVal = []
list_eigVec = []

# for i in range(len(group_name)):
#     name_file = group_name[i] + '.csv';
#     content_file = np.loadtxt(name_file, delimiter=';')
#     list_group.append(content_file)
#     list_mean.append(mean_class(content_file))
#     cov_matrix_temp = cov_matrix(content_file)
#     list_inv_cov.append(inv_matrix(cov_matrix_temp))
#     list_det_cov.append(det(cov_matrix_temp))
#     list_cov.append(cov_matrix_temp)
#     eigVal,eigVec = eigenval_vec(cov_matrix_temp)
#     list_eigVal.append(eigVal)
#     list_eigVec.append(eigVec)

list_data_testing = np.loadtxt('X_training.csv', delimiter=',')
#list_data_testing_name = np.genfromtxt('metal-label_00.csv', dtype='str')

# eigVec = list_eigVec[0];
# for i in range(len(group_name)):
#     #print(i)
#     eV = eigVec[:, i]
#     print(eV)
# for h2 in np.arange(1,100,1):
    #mqdf2_10(group_name,list_mean,list_data_testing,list_data_testing_name,list_eigVal,list_eigVec,h2)
#pp.pprint(eigVec[:,1])

#pp.pprint(list_cov)
#pp.pprint(list_eigVal)
#pp.pprint(list_eigVec)