import numpy as np
import math as m
import pandas as pd
from pandas import DataFrame

def cov_matrix(A, opt=1):
    n = len(A)
    m_iden = np.ones(n)
    a = A - np.dot(m_iden, A) / n
    v = np.dot(np.transpose(a), a) / (n - opt)
    return v

def inv_matrix(A):
    return np.linalg.inv(A)

def mean_class(A):
    return np.mean(A, axis=0)

def det(A):
    return np.linalg.det(A)

def log(N):
    #N = abs(N)
    return m.log10(N)

def calc_prior_probabilities(list_group):
    prior_probalities = []
    size = 0
    for group in list_group:
        c = len(group)
        prior_probalities.append(c)
        size = size + c
    for i in range(len(prior_probalities)):
        n = prior_probalities[i]
        phi = n / size
        log_phi = log(phi)
        prior = [n, phi, log_phi]
        prior_probalities[i] = prior
    return prior_probalities

def eigenval_vec(A):
    return np.linalg.eig(A)

def minor_eigenvalue(eig_val, mode_m_eigval, valk, fix_minor_eig_val):
    retVal = 0.0
    if mode_m_eigval == "SMALLEST":
        retVal = min(eig_val)
    if mode_m_eigval == "AVERAGE":
        d = len(eig_val)
        slice_eig_val = eig_val[valk:d:1]
        sum_eig_val = sum(slice_eig_val)
        retVal = 1 / (d - valk) * sum_eig_val
    if mode_m_eigval == "FIXED":
        retVal = fix_minor_eig_val
    return retVal

# 5 - 16 Discriminative Learning Quadratic Discriminant Function for Handwriting Recognition -- liu2004
def mqdf2_7(group_name, list_mean, list_data_testing, list_data_testing_name, list_eigval, list_eigvec, val_k, h2):
    accuracy = 0.0
    count = 0
    for k in range(len(list_data_testing)):
        data_testing = list_data_testing[k]
        data_testing_name = list_data_testing_name[k]
        g1 = []
        for i in range(len(group_name)):
            a_training = np.array(data_testing)
            a_mean = np.array(list_mean[i])
            a_tr_mean = a_training - a_mean
            eig_val = list_eigval[i]
            eig_vec = list_eigvec[i]
            sort_idx = eig_val.argsort()[::-1]
            eig_val = eig_val[sort_idx]
            eig_vec = eig_vec[:, sort_idx]
            sum_1 = 0.0
            sum_3 = 0.0
            d = len(eig_val)
            delta = minor_eigenvalue(eig_val, "FIXED", valk, h2)
            euclidean_dist = np.linalg.norm(a_tr_mean)
            r1 = pow(euclidean_dist, 2)
            for j in range(val_k):
                eval = eig_val[j]
                evec = eig_vec[:, j]
                sum_1_1 = np.dot(np.transpose(evec), a_tr_mean)
                sum_1 = sum_1 + 1 / eval * pow(sum_1_1, 2)
                r1 = r1 - pow(np.dot(np.transpose(a_tr_mean), evec), 2)
                sum_3 = sum_3 + log(eval)
            sum_4 = (d - val_k) * log(delta)
            g2_7 = sum_1 + 1 / delta * r1 + sum_3 + sum_4
            list_temp = [group_name[i], g2_7]
            g1.append(list_temp)
        # pp.pprint(g1)
        key, value = min(g1, key=lambda x: x[1])
        # print(data_testing_name,';',key, ';',value)
        if (key == data_testing_name):
            count = count + 1
    accuracy = count / len(list_data_testing) * 100
    if accuracy_max < accuracy :
        print("accuracy MQDF2_7 k=",val_k," h2=",h2,":", accuracy, "%")
    # print(hpow2,"\t", accuracy, "%")

f = open("header_03.txt","r")
f1 = f.readlines()
header = f1[0].split(",")
header.append("label")

dt_tr_sp_0 = pd.read_csv('X-training-label.csv', delimiter=',', header=None, names=header, dtype={"label":object})
dt_tr_sp = dt_tr_sp_0.iloc[:,1984:2384]
#dt_tr_sp = dt_tr_sp.multiply(100.0)
dt_tr_sp['label'] = dt_tr_sp_0['label']

list_group = []
list_mean = []
list_inv_cov = []
list_det_cov = []
list_cov = []
list_eigVal = []
list_eigVec = []

group_name = dt_tr_sp.label.unique()
ls_gn = list(group_name)
ls_gn =  [x for x in ls_gn if str(x) != 'nan']
group_name = tuple(ls_gn)
#print(group_name)


for i in range(len(group_name)):
    name_file = group_name[i];
    print(name_file)
    pd3 = dt_tr_sp[dt_tr_sp.label == name_file]
    del pd3['label']
    # content_file = np.loadtxt(name_file, delimiter=';')
    content_file = pd3.to_numpy()
    list_group.append(content_file)
    list_mean.append(mean_class(content_file))
    cov_matrix_temp = cov_matrix(content_file)
    try:
        inv_matrx_temp = inv_matrix(cov_matrix_temp)
    except:
        print(name_file)
        print(content_file)
        print(cov_matrix_temp)
    list_inv_cov.append(inv_matrx_temp)
    list_det_cov.append(det(cov_matrix_temp))
    list_cov.append(cov_matrix_temp)
    eigVal, eigVec = eigenval_vec(cov_matrix_temp)
    list_eigVal.append(eigVal.real)
    list_eigVec.append(eigVec.real)

list_data_testing_name = dt_tr_sp['label'].to_list()
del dt_tr_sp['label']
list_data_testing = dt_tr_sp.to_numpy()

#print(type(list_data_testing_name))
accuracy_max = 0.0
# for valk in range(1,200):
#     for h2 in np.arange(0.01, 1, 0.01):
#         mqdf2_7(group_name,list_mean,list_data_testing,list_data_testing_name,list_eigVal,list_eigVec,valk, h2)