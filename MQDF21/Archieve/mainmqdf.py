import mqdf
import numpy as np
import time as t
from sklearn.metrics import accuracy_score


def main_proses():
    start1 = t.time()
    mqdf_train = mqdf.MqdfTraining("X-training-split-npwk.csv","y-training-split-npwk.csv")
    mqdf_train.importfile()
    x_train, y_train, x_test, y_test = mqdf_train.sub_sampling(0.95)
    group_name = mqdf_train.group_name
    end1 = t.time()
    print("import file : " + str(end1 - start1))

    start1 = t.time()
    mqdf_basic_calc = mqdf.MqdfBasicCalc(x_train,group_name)
    dict_mqdf = mqdf_basic_calc.proses()
    p_d = mqdf_basic_calc.p_d
    end1 = t.time()
    print("basic calc : " + str(end1 - start1))

    start1 = t.time()
    mqdf_classifier = mqdf.MyMqdf(dict_mqdf, group_name, p_d)
    mqdf_classifier.fit(x_test, y_test)
    y_test = mqdf_classifier.my_y_test
    # predictions = mqdf_classifier.predict(1, 0.001)
    # accu = accuracy_score(y_test, predictions) * 100
    # print(accu)
    max_predictions = []
    max_accu = 0.0
    for k in range(1,150):
        for h2 in np.arange(0.001,0.1,0.001):
            start2 = t.time()
            predictions = mqdf_classifier.predict(k, h2)
            accu = accuracy_score(y_test, predictions) * 100
            end2 = t.time()
            if max_accu < accu:
                print("MQDF2_7; k=" + str(k) + ";h2=" + str(round(h2, 4)) + ";a=" + str(round(accu, 4))
                    + "%;time=" + str(round(end2-start2, 4)))
                max_accu = accu
                max_predictions = predictions
    end1 = t.time()
    print("classifier : " + str(end1 - start1))
    p_y_test = len(y_test)
    for i in range(p_y_test):
        print(y_test[i] + ";" + max_predictions[i])

if __name__ == "__main__":
    main_proses()