import numpy as np
import math as m
import pandas as pd
import gc
import time as t
import datetime as dtt
import pprint as pp

def cov_matrix(a0, opt=1):
    n = len(a0)
    m_iden = np.ones(n)
    a = a0 - np.dot(m_iden, a0) / n
    v = np.dot(np.transpose(a), a) / (n - opt)
    return v


def inv_matrix(a0):
    return np.linalg.inv(a0)


def mean_class(a0):
    return np.mean(a0, axis=0)


def det(a0):
    return np.linalg.det(a0)


def log(n):
    n = abs(n)
    return m.log10(n)


def calc_prior_probabilities(local_list_group):
    prior_probabilities = []
    size = 0
    for group in local_list_group:
        c = len(group)
        prior_probabilities.append(c)
        size = size + c
    for loc_i in range(len(prior_probabilities)):
        n = prior_probabilities[loc_i]
        phi = n / size
        log_phi = log(phi)
        prior = [n, phi, log_phi]
        prior_probabilities[loc_i] = prior
    return prior_probabilities


def calc_eigen_val_vec(a0):
    return np.linalg.eig(a0)


def minor_eigenvalue(eig_val, mode_m_eig_val, val1_k, fix_minor_eig_val):
    ret_val = 0.0
    if mode_m_eig_val == "SMALLEST":
        ret_val = min(eig_val)
    if mode_m_eig_val == "AVERAGE":
        d = len(eig_val)
        slice_eig_val = eig_val[val1_k:d:1]
        sum_eig_val = sum(slice_eig_val)
        ret_val = 1 / (d - val1_k) * sum_eig_val
    if mode_m_eig_val == "FIXED":
        ret_val = fix_minor_eig_val
    return ret_val


# 5 - 16 Discriminative Learning Quadratic Discriminant Function for Handwriting Recognition -- liu2004
#@profile
def mqdf2_7(loc1_group_name, loc1_list_mean, loc1_list_data_testing, loc1_list_data_testing_name, loc1_list_eig_val,
            loc1_list_eig_vec, loc1_val_k, loc1_h2, loc1_accuracy_max):
    start2 = t.time()
    count = 0
    for k in range(len(loc1_list_data_testing)):
        data_testing = loc1_list_data_testing[k]
        data_testing_name = loc1_list_data_testing_name[k]
        g1 = []
        for loc1_i in range(len(loc1_group_name)):
            a_training = data_testing
            a_mean = np.array(loc1_list_mean[loc1_i])
            a_tr_mean = a_training - a_mean
            eig_val = loc1_list_eig_val[loc1_i]
            eig_vec = loc1_list_eig_vec[loc1_i]
            sum_1 = 0.0
            sum_3 = 0.0
            d = len(eig_val)
            delta = minor_eigenvalue(eig_val, "FIXED", loc1_val_k, loc1_h2)
            euclidean_dist = np.linalg.norm(a_tr_mean)
            r1 = pow(euclidean_dist, 2)
            for j in range(loc1_val_k):
                e_val = eig_val[j]
                e_vec = eig_vec[:, j]
                sum_1_1 = np.dot(np.transpose(e_vec), a_tr_mean)
                sum_1 = sum_1 + 1 / e_val * pow(sum_1_1, 2)
                r1 = r1 - pow(np.dot(np.transpose(a_tr_mean), e_vec), 2)
                sum_3 = sum_3 + log(e_val)
            sum_4 = (d - loc1_val_k) * log(delta)
            g2_7 = sum_1 + 1 / delta * r1 + sum_3 + sum_4
            #d_sum_temp = {sum_1, delta, r1, sum_3, sum_4}
            #print(d_sum_temp)
            list_temp = [loc1_group_name[loc1_i], g2_7]
            g1.append(list_temp)
        #pp.pprint(g1)
        key, value = min(g1, key=lambda x: x[1])
        # print(data_testing_name,';',key, ';',value)
        if key == data_testing_name:
            count = count + 1
    accuracy = count / len(loc1_list_data_testing) * 100
    end2 = t.time()
    proses = end2 - start2
    print("accuracy MQDF2_7 k=", loc1_val_k, ";h2=", loc1_h2, ";a=", accuracy, "%;time=", proses)
    if loc1_accuracy_max < accuracy:
        loc1_accuracy_max = accuracy
    # print("ed :", euclidean_dist, "\na_m :", a_tr_mean)
    return loc1_accuracy_max


gc.disable()
start = t.time()
print("start process ... ", dtt.datetime.now())
X = "X-training-split.csv"
f = open("header_03.txt", "r")
f1 = f.readlines()
header = f1[0].split(",")
header.append("label")
f.close()

dt_tr_sp_0 = pd.read_csv(X, delimiter=',', header=None, names=header, dtype={"label": object})
start_ind_col = 1984
end_ind_col = 2384
p_col = end_ind_col - start_ind_col
dt_tr_sp = dt_tr_sp_0.iloc[:, start_ind_col:end_ind_col]
dt_tr_sp['label'] = dt_tr_sp_0['label']
del dt_tr_sp_0, f1, header, f
end1 = t.time()
print("import file : " + str(end1 - start))

start = t.time()
list_group = []
list_mean = []
list_inv_cov = []
list_det_cov = []
list_cov = []
list_eigVal = []
list_eigVec = []

group_name = dt_tr_sp.label.unique()
ls_gn = list(group_name)
ls_gn = [x for x in ls_gn if str(x) != 'nan']
group_name = tuple(ls_gn)
# print(group_name)


for i in range(len(group_name)):
    name_file = group_name[i]
    # print(name_file)
    pd3 = dt_tr_sp[dt_tr_sp.label == name_file]
    del pd3['label']
    # content_file = np.loadtxt(name_file, delimiter=';')
    content_file = pd3.to_numpy()
    list_group.append(content_file)
    list_mean.append(mean_class(content_file))
    cov_matrix_temp = cov_matrix(content_file)
    try:
        inv_matrix_temp = inv_matrix(cov_matrix_temp)
        list_inv_cov.append(inv_matrix_temp)
        del inv_matrix_temp
    except:
        print(name_file)
        # print(content_file)
        # print(cov_matrix_temp)
    list_det_cov.append(det(cov_matrix_temp))
    list_cov.append(cov_matrix_temp)
    eigVal, eigVec = calc_eigen_val_vec(cov_matrix_temp)
    list_eigVal.append(eigVal.real)
    list_eigVec.append(eigVec.real)
    del content_file, cov_matrix_temp, eigVal, eigVec, name_file, pd3
end1 = t.time()
print("proses mean, eigVec, eigVal : " + str(end1 - start))

list_data_testing_name = dt_tr_sp['label'].to_list()
del dt_tr_sp['label']
list_data_testing = dt_tr_sp.to_numpy()

accuracy_max = 0.0
np_list_group_name = np.asarray(group_name, dtype=np.str)
np_list_mean = np.asarray(list_mean, dtype=np.float64)
np_list_data_testing = np.asarray(list_data_testing, dtype=np.float64)
np_list_data_testing_name = np.asarray(list_data_testing_name, dtype=np.str)
np_list_eigVal = np.asarray(list_eigVal, dtype=np.float64)
np_list_eigVec = np.asarray(list_eigVec, dtype=np.float64)

del list_group, list_det_cov, list_inv_cov, list_cov, dt_tr_sp,
del group_name, list_mean, list_data_testing, list_data_testing_name, list_eigVal, list_eigVec, ls_gn

# MODE = "TESTING"
# print("start mqdf2... ")
#
# if MODE == "TRAINING":
#     for val_k in range(1, 50):
#         for h2 in np.arange(0.01, 1, 0.01):
#             if accuracy_max != 100.0:
#                 accuracy_max = mqdf2_7(np_list_group_name, np_list_mean, np_list_data_testing,
#                                        np_list_data_testing_name, np_list_eigVal, np_list_eigVec, val_k, h2,
#                                        accuracy_max)
# #         mqdf2_7(group_name,list_mean,list_data_testing,list_data_testing_name,list_eigVal,list_eigVec,valk, h2)
# elif MODE == "TESTING":
#     val_k = 2
#     h2 = 0.6
#     mqdf2_7(np_list_group_name, np_list_mean, np_list_data_testing, np_list_data_testing_name, np_list_eigVal,
#             np_list_eigVec, val_k, h2, accuracy_max)

MODE = "TRAINING"
print("start mqdf2... ", dtt.datetime.now())
start = t.time()
if MODE == "TRAINING":
    for val_k in range(1, 50):
        for h2 in np.arange(0.01, 1, 0.01):
            if accuracy_max != 100.0:
                accuracy_max = mqdf2_7(np_list_group_name, np_list_mean, np_list_data_testing,
                                       np_list_data_testing_name, np_list_eigVal, np_list_eigVec, val_k, h2,
                                       accuracy_max)
elif MODE == "TESTING 1":
    val_k = 1
    h2 = 0.6
    mqdf2_7(np_list_group_name, np_list_mean, np_list_data_testing, np_list_data_testing_name, np_list_eigVal,
            np_list_eigVec, val_k, h2, accuracy_max)
elif MODE == "TESTING 2":
    for val_k in range(1, 5):
        for h2 in np.arange(0.01, 1, 0.01):
            mqdf2_7(np_list_group_name, np_list_mean, np_list_data_testing, np_list_data_testing_name,
                                   np_list_eigVal, np_list_eigVec, val_k, h2, accuracy_max)
end1 = t.time()
print("mqdf2 finish : " + str(end1 - start))