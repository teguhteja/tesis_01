import logging as my_log

my_log.basicConfig(filename='app.log', filemode='w', format='%(name)s - %(levelname)s - %(message)s')
my_log.warning('This will get logged to a file')


def test_write_log():
    my_log.warning('This warning will get logged to a file')


test_write_log()
