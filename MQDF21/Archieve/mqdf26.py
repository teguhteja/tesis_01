import numpy as np
import pandas as pd
import gc
import time as t
import datetime as dtt
from mqdf.MqdfBasic import mean_class, inv_matrix, cov_matrix, calc_eigen_val_vec
import tensorflow as tf
# tr = training, te = testing, tmp = temporary, gr = group, nm = name, tp = transpose

pow2 = tf.constant([2.0], dtype=tf.double)
one = tf.constant([-1.0], dtype=tf.double)
dict_matrix = {}


def con_np(arg, np_type):
    return tf.convert_to_tensor(arg, dtype=np_type)


def log10(x):
    x = abs(x)
    numerator = tf.math.log(x)
    denominator = tf.math.log(tf.constant(10, dtype=numerator.dtype))
    return numerator / denominator


def mqdf2_7(group_nm, l_mean, l_dt_test, l_dt_test_nm, l_eig_val, l_eig_vec, lo_k, lo_h2, acc_max, d):
    start2 = t.time()
    count = 0
    for x in range(len(l_dt_test)):
        dt_tr = l_dt_test[x]
        dt_te_nm = l_dt_test_nm[x]
        acc_tmp = float("inf")
        nm_gr_tmp = ""
        #  g2_7 = 0.0
        for w in range(len(group_nm)):
            dt_mean = l_mean[w]
            eig_vec_w = l_eig_vec[w]
            eig_val_w = l_eig_val[w]
            dt_tr_s_mean = tf.subtract(dt_tr, dt_mean, name=None)
            dt_tr_s_mean = tf.reshape(dt_tr_s_mean, shape=[len(dt_tr_s_mean), 1])
            eiv_m_tr_mean = tf.matmul(eig_vec_w, dt_tr_s_mean, transpose_a=True)
            pow2_eiv_tr_mean = tf.pow(eiv_m_tr_mean, pow2)
            one_ei_val = tf.pow(eig_val_w, one)
            one_ei_val = tf.reshape(one_ei_val, shape=[len(one_ei_val), 1])
            sp_eiv_pow_eiv = tf.linalg.matmul(one_ei_val, pow2_eiv_tr_mean, transpose_b=True)
            diag_sp_eig_val_vec = tf.linalg.tensor_diag_part(sp_eiv_pow_eiv)
            slice_diag = tf.slice(diag_sp_eig_val_vec, [0], [lo_k])
            sum1 = tf.reduce_sum(slice_diag)
            norm_tr_s_me = tf.norm(dt_tr_s_mean)
            pow2_norm = tf.pow(norm_tr_s_me, pow2)
            pow2_eiv_tr_mean = tf.reshape(pow2_eiv_tr_mean, shape=[len(pow2_eiv_tr_mean), ])
            slice_eiv_m_tr_mean = tf.slice(pow2_eiv_tr_mean, [0], [lo_k])
            sum_sl_eiv = tf.reduce_sum(slice_eiv_m_tr_mean)
            sum2 = tf.subtract(pow2_norm, sum_sl_eiv)
            sum2 = tf.multiply(100.0, sum2)  # adjustment 100x
            tf_log_eig_val = log10(eig_val_w)
            slice_log_eig_val = tf.slice(tf_log_eig_val, [0], [lo_k])
            sum3 = tf.reduce_sum(slice_log_eig_val)
            tf_d_k = tf.constant((d-lo_k), dtype=tf.float64)
            sum4 = tf.math.multiply(tf_d_k, log10(lo_h2))
            g2_7 = tf.math.add(tf.math.add(sum1, sum2), tf.math.add(sum3, sum4))
            #  tf.print(sum1, sum2, sum3, sum4, d, lo_k, lo_h2)
            #  tf.print(acc_tmp, g2_7)
            if tf.math.greater(acc_tmp,g2_7):
                nm_gr_tmp = group_nm[w]
                acc_tmp = g2_7
        #  print(dt_te_nm, " : ", nm_gr_tmp, " : ",g2_7)
        if nm_gr_tmp == dt_te_nm:
            count = count + 1
    accuracy = count / len(l_dt_test) * 100
    end2 = t.time()
    proses = end2 - start2
    print("MQDF2_7 k=", lo_k, ";h2=", round(lo_h2, 4), ";a=", round(accuracy, 4), "%;time=", round(proses, 4))
    if acc_max < accuracy:
        acc_max = accuracy
    return acc_max, accuracy, proses


def mqdf2_7_dict(group_nm, l_mean, l_dt_test, l_dt_test_nm, l_eig_val, l_eig_vec, lo_k, lo_h2, acc_max, d):
    start2 = t.time()
    count = 0
    global dict_matrix
    for x in range(len(l_dt_test)):
        dt_tr = l_dt_test[x]
        dt_te_nm = l_dt_test_nm[x]
        acc_tmp = float("inf")
        nm_gr_tmp = ""
        #  g2_7 = 0.0
        for w in range(len(group_nm)):
            key_d_m = ("diag_sp_eig_val_vec", group_nm[w])
            key_d_m1 = ("pow2_eiv_tr_mean", group_nm[w])
            key_d_m2 = ("tf_log_eig_val", group_nm[w])
            key_d_m3 = ("sum4", group_nm[w])
            if dict_matrix not in key_d_m:
                dt_mean = l_mean[w]
                eig_vec_w = l_eig_vec[w]
                eig_val_w = l_eig_val[w]
                dt_tr_s_mean = tf.subtract(dt_tr, dt_mean)
                dt_tr_s_mean = tf.reshape(dt_tr_s_mean, shape=[len(dt_tr_s_mean), 1])
                eiv_m_tr_mean = tf.matmul(eig_vec_w, dt_tr_s_mean, transpose_a=True)
                pow2_eiv_tr_mean = tf.pow(eiv_m_tr_mean, pow2)
                one_ei_val = tf.pow(eig_val_w, one)
                one_ei_val = tf.reshape(one_ei_val, shape=[len(one_ei_val), 1])
                sp_eiv_pow_eiv = tf.linalg.matmul(one_ei_val, pow2_eiv_tr_mean, transpose_b=True)
                diag_sp_eig_val_vec = tf.linalg.tensor_diag_part(sp_eiv_pow_eiv)
                dict_matrix[key_d_m] = diag_sp_eig_val_vec

                norm_tr_s_me = tf.norm(dt_tr_s_mean)
                pow2_norm = tf.pow(norm_tr_s_me, pow2)
                pow2_eiv_tr_mean = tf.reshape(pow2_eiv_tr_mean, shape=[len(pow2_eiv_tr_mean), ])
                dict_matrix[key_d_m1] = pow2_eiv_tr_mean

                tf_log_eig_val = log10(eig_val_w)
                dict_matrix[key_d_m2] = tf_log_eig_val

                tf_d_k = tf.constant((d - lo_k), dtype=tf.float64)
                sum4 = tf.math.multiply(tf_d_k, log10(lo_h2))
                dict_matrix[key_d_m3] = sum4
            else:
                diag_sp_eig_val_vec = dict_matrix[key_d_m]
                pow2_eiv_tr_mean = dict_matrix[key_d_m1]
                tf_log_eig_val = dict_matrix[key_d_m2]
                sum4 = dict_matrix[key_d_m3]

            slice_diag = tf.slice(diag_sp_eig_val_vec, [0], [lo_k])
            sum1 = tf.reduce_sum(slice_diag)

            slice_eiv_m_tr_mean = tf.slice(pow2_eiv_tr_mean, [0], [lo_k])
            sum_sl_eiv = tf.reduce_sum(slice_eiv_m_tr_mean)
            sum2 = tf.subtract(pow2_norm, sum_sl_eiv)
            sum2 = tf.multiply(100.0, sum2)  # adjustment 100x

            slice_log_eig_val = tf.slice(tf_log_eig_val, [0], [lo_k])
            sum3 = tf.reduce_sum(slice_log_eig_val)

            g2_7 = tf.math.add(tf.math.add(sum1, sum2), tf.math.add(sum3, sum4))
            #  tf.print(sum1, sum2, sum3, sum4, d, lo_k, lo_h2)
            #  tf.print(acc_tmp, g2_7)
            if tf.math.greater(acc_tmp,g2_7):
                nm_gr_tmp = group_nm[w]
                acc_tmp = g2_7
        #  print(dt_te_nm, " : ", nm_gr_tmp, " : ",g2_7)
        if nm_gr_tmp == dt_te_nm:
            count = count + 1
    accuracy = count / len(l_dt_test) * 100
    end2 = t.time()
    proses = end2 - start2
    print("MQDF2_7 k=", lo_k, ";h2=", round(lo_h2, 4), ";a=", round(accuracy, 4), "%;time=", round(proses, 4))
    if acc_max < accuracy:
        acc_max = accuracy
    return acc_max, accuracy, proses


gc.disable()
start = t.time()
print("start init ... ", dtt.datetime.now())
X = "X-training-label.csv"
X_te = "X-testing-label_01.csv"
f = open("header_03.txt", "r")
f1 = f.readlines()
header = f1[0].split(",")
header.append("label")
f.close()

dt_tr_sp_0 = pd.read_csv(X_te, delimiter=',', header=None, names=header, dtype={"label": object})
dt_te_sp_0 = pd.read_csv(X_te, delimiter=',', header=None, names=header, dtype={"label": object})
start_ind_col = 1984
end_ind_col = 2384
p_col = end_ind_col - start_ind_col
dt_tr_sp = dt_tr_sp_0.iloc[:, start_ind_col:end_ind_col]
dt_te_sp = dt_te_sp_0.iloc[:, start_ind_col:end_ind_col]
dt_tr_sp['label'] = dt_tr_sp_0['label']
#  p_col = dt_tr_sp_0.shape[1]
del dt_tr_sp_0
end1 = t.time()
print("import file : " + str(end1 - start))

start = t.time()
list_group = []
list_mean = []
list_inv_cov = []
list_det_cov = []
list_cov = []
list_eigVal = []
list_eigVec = []

group_name = dt_tr_sp.label.unique()
ls_gn = list(group_name)
ls_gn = [x for x in ls_gn if str(x) != 'nan']
group_name = tuple(ls_gn)
# print(group_name)

for i in range(len(group_name)):
    name_file = group_name[i]
    # print(name_file)
    pd3 = dt_tr_sp[dt_tr_sp.label == name_file]
    del pd3['label']
    # content_file = np.loadtxt(name_file, delimiter=';')
    content_file = pd3.to_numpy()
    list_group.append(content_file)
    list_mean.append(con_np(mean_class(content_file), tf.float64))
    cov_matrix_temp = cov_matrix(content_file)
    try:
        inv_matrix_temp = inv_matrix(cov_matrix_temp)
        list_inv_cov.append(inv_matrix_temp)
        del inv_matrix_temp
    except:
        print(name_file)
        # print(content_file)
        # print(cov_matrix_temp)
    #  list_det_cov.append(det(cov_matrix_temp))
    #  list_cov.append(con_np(cov_matrix_temp,tf.float64))
    eigVal, eigVec = calc_eigen_val_vec(cov_matrix_temp)
    list_eigVal.append(con_np(eigVal.real, tf.float64))
    list_eigVec.append(con_np(eigVec.real, tf.float64))
    del content_file, cov_matrix_temp, eigVal, eigVec, name_file, pd3
end1 = t.time()
print("proses mean, eigVec, eigVal : " + str(end1 - start))

list_data_testing_name = dt_tr_sp['label'].to_list()
del dt_tr_sp['label']
list_data_testing = dt_tr_sp.to_numpy()

accuracy_max = 0.0
# group_name = np.asarray(group_name, dtype=np.str)
# list_mean = np.asarray(list_mean, dtype=np.float64)
# dt_testing = con_np(list_data_testing, tf.float64)
# dt_testing_name = con_np(list_data_testing_name, tf.string)
# np_eigVal = con_np(list_eigVal, tf.float64)
# np_eigVec = con_np(list_eigVec, tf.float64)

#  del group_name, list_mean, list_data_testing, list_data_testing_name, list_eigVal, list_eigVec
del list_group, ls_gn, list_det_cov, list_inv_cov, list_cov, dt_tr_sp

MODE = "TRAINING"
print("start mqdf2... ", dtt.datetime.now())
start = t.time()
name_file_log = "MQDF2_" + MODE + "_" + str(dtt.datetime.now()) + ".txt"
#  f = open(name_file_log, "w+")
if MODE == "TRAINING":
    for val_k in range(9, 10):
        for h2 in np.arange(0.01, 1.00, 0.01):
            if accuracy_max != 100.0:
                get_val = mqdf2_7(group_name, list_mean, list_data_testing, list_data_testing_name, list_eigVal,
                                  list_eigVec, val_k, h2, accuracy_max, p_col)
                accuracy_max = get_val[0]
             #  f.writelines("\nMQDF2_7; k=" + str(val_k) + ";h2=" + str(round(h2,4)) + ";a=" + str(round(get_val[1],4))
             #                + "%;time=" + str(round(get_val[2],4)))
end1 = t.time()
print("mqdf2 finish : " + str(end1 - start))
#  f.close()
