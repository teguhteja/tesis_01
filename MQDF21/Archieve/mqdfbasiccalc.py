from mqdf.MqdfBasic import *

class MqdfBasicCalc:
    DictMqdfBasic = {}
    p_d = 0
    def __init__(self, x_train, group_name):
        self.x_train = x_train
        self.group_name = group_name

    #calc data train to get mean class, eigen value and vector
    def proses(self):
        # list_group = []
        # list_mean = []
        # list_inv_cov = []
        # list_det_cov = []
        # list_cov = []
        # list_eigVal = []
        # list_eigVec = []

        p_group_name = len(self.group_name)
        for i in range(p_group_name):
            name_class = self.group_name[i]
            # print(name_file)
            # pd3 = dt_tr_sp[dt_tr_sp.label == name_file]
            # del pd3['label']
            # content_file = np.loadtxt(name_file, delimiter=';')
            class_i = self.x_train[i]
            np_class_i = class_i.to_numpy()
            #list_group.append(np_class_i)
            key_class = (name_class,"mean")
            mean_class_i = mean_class(np_class_i)
            self.DictMqdfBasic[key_class] = mean_class_i
            #list_mean.append()
            cov_matrix_temp = cov_matrix(np_class_i)
            # try:
            #     inv_matrix_temp = inv_matrix(cov_matrix_temp)
            #     list_inv_cov.append(inv_matrix_temp)
            #     del inv_matrix_temp
            # except:
            #     print(name_class)
            #     # print(content_file)
            #     # print(cov_matrix_temp)
            # list_det_cov.append(det(cov_matrix_temp))
            # list_cov.append(cov_matrix_temp)
            eig_val, eig_vec = calc_eigen_val_vec(cov_matrix_temp)
            key_class = (name_class,"eig_val")
            self.DictMqdfBasic[key_class] = eig_val
            key_class = (name_class, "eig_vec")
            self.DictMqdfBasic[key_class] = eig_vec
            #list_eigVal.append(eig_val.real)
            #list_eigVec.append(eig_vec.real)
            #del np_class_i, cov_matrix_temp, eig_val, eig_vec, name_class
            p_d = len(eig_val)
        return self.DictMqdfBasic