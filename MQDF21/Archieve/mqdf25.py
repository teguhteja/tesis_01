import numpy as np
import math as m
import pandas as pd
import gc
import time as t
import datetime as dtt
import linecache
import os
import tracemalloc
# import pprint as pp
# numpy dengan dictionary


def display_top(snapshot, key_type='lineno', limit=3):
    snapshot = snapshot.filter_traces((
        tracemalloc.Filter(False, "<frozen importlib._bootstrap>"),
        tracemalloc.Filter(False, "<unknown>"),
    ))
    top_stats = snapshot.statistics(key_type)

    print("Top %s lines" % limit)
    for index, stat in enumerate(top_stats[:limit], 1):
        frame = stat.traceback[0]
        # replace "/path/to/module/file.py" with "module/file.py"
        filename = os.sep.join(frame.filename.split(os.sep)[-2:])
        print("#%s: %s:%s: %.1f KiB"
              % (index, filename, frame.lineno, stat.size / 1024))
        line = linecache.getline(frame.filename, frame.lineno).strip()
        if line:
            print('    %s' % line)

    other = top_stats[limit:]
    if other:
        size = sum(stat.size for stat in other)
        print("%s other: %.1f KiB" % (len(other), size / 1024))
    total = sum(stat.size for stat in top_stats)
    print("Total allocated size: %.1f KiB" % (total / 1024))


def cov_matrix(a0, opt=1):
    n = len(a0)
    m_iden = np.ones(n)
    a = a0 - np.dot(m_iden, a0) / n
    v = np.dot(np.transpose(a), a) / (n - opt)
    return v


def inv_matrix(a0):
    return np.linalg.inv(a0)


def mean_class(a0):
    return np.mean(a0, axis=0)


def det(a0):
    return np.linalg.det(a0)


def log(n):
    n = abs(n)
    return m.log10(n)


def calc_prior_probabilities(local_list_group):
    prior_probabilities = []
    size = 0
    for group in local_list_group:
        c = len(group)
        prior_probabilities.append(c)
        size = size + c
    for loc_i in range(len(prior_probabilities)):
        n = prior_probabilities[loc_i]
        phi = n / size
        log_phi = log(phi)
        prior = [n, phi, log_phi]
        prior_probabilities[loc_i] = prior
    return prior_probabilities


def calc_eigen_val_vec(a0):
    return np.linalg.eig(a0)


def minor_eigenvalue(eig_val, mode_m_eig_val, val1_k, fix_minor_eig_val):
    ret_val = 0.0
    if mode_m_eig_val == "SMALLEST":
        ret_val = min(eig_val)
    if mode_m_eig_val == "AVERAGE":
        d = len(eig_val)
        slice_eig_val = eig_val[val1_k:d:1]
        sum_eig_val = sum(slice_eig_val)
        ret_val = 1 / (d - val1_k) * sum_eig_val
    if mode_m_eig_val == "FIXED":
        ret_val = fix_minor_eig_val
    return ret_val


dict_a_tr_mean = {}
dict_r1 = {}
dict_sum = {}
eul = m.e


# 5 - 16 Discriminative Learning Quadratic Discriminant Function for Handwriting Recognition -- liu2004
#@profile
def mqdf2_7(loc1_group_name, loc1_list_mean, loc1_list_data_testing, loc1_list_data_testing_name, loc1_list_eig_val,
            loc1_list_eig_vec, loc1_val_k, delta, loc1_accuracy_max, loc1_list_delta, d):
    start2 = t.time()
    count = 0
    global dict_a_tr_mean, dict_r1, dict_sum
    loc1_dict_a_tr_mean = dict_a_tr_mean
    loc1_dict_r1 = dict_r1
    loc1_dict_sum = dict_sum
    for k in range(len(loc1_list_data_testing)):
        a_training = loc1_list_data_testing[k]
        data_testing_name = loc1_list_data_testing_name[k]
#        g1 = []
        acc_temp = float("inf")
        name_group_temp = ""
        for loc1_i in range(len(loc1_group_name)):
            a_mean = loc1_list_mean[loc1_i]
            key_d_a_tr_m = (k, loc1_i)
            # delta = loc1_list_delta[loc1_i]
            if key_d_a_tr_m not in loc1_dict_a_tr_mean:
                a_tr_mean = a_training - a_mean
                loc1_dict_a_tr_mean[key_d_a_tr_m] = a_tr_mean
                euclidean_dist = np.linalg.norm(a_tr_mean)
                r1 = pow(euclidean_dist, 2)
                loc1_dict_r1[key_d_a_tr_m] = r1
            else:
                a_tr_mean = loc1_dict_a_tr_mean[key_d_a_tr_m]
                r1 = loc1_dict_r1[key_d_a_tr_m]
            eig_val = loc1_list_eig_val[loc1_i]
            eig_vec = loc1_list_eig_vec[loc1_i]
            sum_1 = 0.0
            sum_3 = 0.0
#            d = len(eig_val)
#            delta = loc1_h2
            key_sum_all = (k, loc1_i, loc1_val_k)
            if key_sum_all not in loc1_dict_sum:
                if loc1_val_k-1 > 0:
                    prev_key_f_val = (k, loc1_i, loc1_val_k-1)
                    prev_val_sum = loc1_dict_sum[prev_key_f_val]
                    sum_1 = prev_val_sum[0]
                    sum_3 = prev_val_sum[1]
                    r1 = prev_val_sum[2]
                j = loc1_val_k - 1
                e_val = eig_val[j]
                e_vec = eig_vec[:, j]
                sum_1_1 = np.dot(np.transpose(e_vec), a_tr_mean)
                sum_1 = sum_1 + 1 / e_val * pow(sum_1_1, 2)
#                sum2 = np.dot(np.transpose(a_tr_mean), e_vec)
                r1 = r1 - pow(sum_1_1, 2)
                sum_3 = sum_3 + log(e_val)
                loc1_dict_sum[key_sum_all] = (sum_1, sum_3, r1)
            else:
                val_sum = loc1_dict_sum[key_sum_all]
                sum_1 = val_sum[0]
                sum_3 = val_sum[1]
                r1 = val_sum[2]
            sum_4 = (d - loc1_val_k) * log(delta)
            g2_7 = sum_1 + 1 / delta * r1 + sum_3 + sum_4
            # d_sum_temp = {sum_1,  1 / delta * r1, sum_3, sum_4}
            # print(d_sum_temp)
            # list_temp = [loc1_group_name[loc1_i], g2_7]
            # g1.append(list_temp)
            # print(sum_1, " [",  1 / delta * r1, "] [", sum_3,"] ", sum_4, d, loc1_val_k, delta)
            if g2_7 < acc_temp:
                name_group_temp = loc1_group_name[loc1_i]
                acc_temp = g2_7
#        pp.pprint(g1)
#        key, value = min(g1, key=lambda x: x[1])
#        print(data_testing_name,';',key, ';', value)
        if name_group_temp == data_testing_name:
            count = count + 1
    accuracy = count / len(loc1_list_data_testing) * 100
    end2 = t.time()
    proses = end2 - start2
    print("MQDF2_7 k=", loc1_val_k, ";h2=", round(delta, 4), ";a=", round(accuracy, 4), "%;time=", round(proses, 4))
    if loc1_accuracy_max < accuracy:
        loc1_accuracy_max = accuracy
    # print("ed :", euclidean_dist, "\na_m :", a_tr_mean)
    return loc1_accuracy_max, accuracy, proses


# 5 - 16 Discriminative Learning Quadratic Discriminant Function for Handwriting Recognition -- liu2004
def dlqdf25(loc1_group_name, loc1_list_mean, loc1_list_data_testing, loc1_list_data_testing_name, loc1_list_eig_val,
            loc1_list_eig_vec, loc1_val_k, delta, loc1_accuracy_max, loc1_list_delta, d):
    start2 = t.time()
    count = 0
    global dict_a_tr_mean, dict_r1, dict_sum
    loc1_dict_a_tr_mean = dict_a_tr_mean
    loc1_dict_r1 = dict_r1
    loc1_dict_sum = dict_sum
    for k in range(len(loc1_list_data_testing)):
        a_training = loc1_list_data_testing[k]
        data_testing_name = loc1_list_data_testing_name[k]
#        g1 = []
        acc_temp = float("inf")
        name_group_temp = ""
        for loc1_i in range(len(loc1_group_name)):
            a_mean = loc1_list_mean[loc1_i]
            key_d_a_tr_m = (k, loc1_i)
            delta = loc1_list_delta[loc1_i]
            if key_d_a_tr_m not in loc1_dict_a_tr_mean:
                a_tr_mean = a_training - a_mean
                loc1_dict_a_tr_mean[key_d_a_tr_m] = a_tr_mean
                euclidean_dist = np.linalg.norm(a_tr_mean)
                r1 = pow(euclidean_dist, 2)
                loc1_dict_r1[key_d_a_tr_m] = r1
            else:
                a_tr_mean = loc1_dict_a_tr_mean[key_d_a_tr_m]
                r1 = loc1_dict_r1[key_d_a_tr_m]
            eig_val = loc1_list_eig_val[loc1_i]
            eig_vec = loc1_list_eig_vec[loc1_i]
            sum_1 = 0.0
            sum_3 = 0.0
#            d = len(eig_val)
#            delta = loc1_h2
            key_sum_all = (k, loc1_i, loc1_val_k)
            if key_sum_all not in loc1_dict_sum:
                if loc1_val_k-1 > 0:
                    prev_key_f_val = (k, loc1_i, loc1_val_k-1)
                    prev_val_sum = loc1_dict_sum[prev_key_f_val]
                    sum_1 = prev_val_sum[0]
                    sum_3 = prev_val_sum[1]
                    r1 = prev_val_sum[2]
                j = loc1_val_k - 1
                e_val = eig_val[j]
                e_vec = eig_vec[:, j]
                sum_1_1 = np.dot(np.transpose(e_vec), a_tr_mean)
                sum_1 = sum_1 + pow(eul,-1*log(e_val))  * pow(sum_1_1, 2)
#                sum2 = np.dot(np.transpose(a_tr_mean), e_vec)
                r1 = r1 - pow(sum_1_1, 2)
                sum_3 = sum_3 + log(e_val)
                loc1_dict_sum[key_sum_all] = (sum_1, sum_3, r1)
            else:
                val_sum = loc1_dict_sum[key_sum_all]
                sum_1 = val_sum[0]
                sum_3 = val_sum[1]
                r1 = val_sum[2]
            sum_4 = (d - loc1_val_k) * log(delta)
            sum_2 = pow(eul,-1*log(delta)) * r1
            g2_7 = sum_1 + sum_2 + sum_3 + sum_4
            # d_sum_temp = {sum_1,  1 / delta * r1, sum_3, sum_4}
            # print(d_sum_temp)
            # list_temp = [loc1_group_name[loc1_i], g2_7]
            # g1.append(list_temp)
            # print(sum_1, " [",  1 / delta * r1, "] [", sum_3,"] ", sum_4, d, loc1_val_k, delta)
            if g2_7 < acc_temp:
                name_group_temp = loc1_group_name[loc1_i]
                acc_temp = g2_7
#        pp.pprint(g1)
#        key, value = min(g1, key=lambda x: x[1])
#        print(data_testing_name,';',key, ';', value)
        if name_group_temp == data_testing_name:
            count = count + 1
    accuracy = count / len(loc1_list_data_testing) * 100
    end2 = t.time()
    proses = end2 - start2
    print("MQDF2_7 k=", loc1_val_k, ";h2=", round(delta, 4), ";a=", round(accuracy, 4), "%;time=", round(proses, 4))
    if loc1_accuracy_max < accuracy:
        loc1_accuracy_max = accuracy
    # print("ed :", euclidean_dist, "\na_m :", a_tr_mean)
    return loc1_accuracy_max, accuracy, proses


tracemalloc.start()
gc.disable()
start = t.time()
print("start init ... ", dtt.datetime.now())
X = "X-training-label.csv"
X_te = "X-testing-label_01.csv"
f = open("header_03.txt", "r")
f1 = f.readlines()
header = f1[0].split(",")
header.append("label")
f.close()

dt_tr_sp_0 = pd.read_csv(X, delimiter=',', header=None, names=header, dtype={"label": object})
dt_te_sp_0 = pd.read_csv(X, delimiter=',', header=None, names=header, dtype={"label": object})
start_ind_col = 1984
end_ind_col = 2384
p_col = end_ind_col - start_ind_col
dt_tr_sp = dt_tr_sp_0.iloc[:, start_ind_col:end_ind_col]
dt_te_sp = dt_te_sp_0.iloc[:, start_ind_col:end_ind_col]
dt_tr_sp['label'] = dt_tr_sp_0['label']
dt_te_sp['label'] = dt_te_sp_0['label']
del dt_tr_sp_0, dt_te_sp_0, f1, header, f
end1 = t.time()
print("import file : " + str(end1 - start))

start = t.time()
list_group = []
list_mean = []
list_inv_cov = []
list_det_cov = []
list_cov = []
list_eigVal = []
list_eigVec = []
list_delta = []
list_avg_delta = []

group_name = dt_tr_sp.label.unique()
ls_gn = list(group_name)
ls_gn = [x for x in ls_gn if str(x) != 'nan']
group_name = tuple(ls_gn)
# print(group_name)

for i in range(len(group_name)):
    name_file = group_name[i]
    # print(name_file)
    pd3 = dt_tr_sp[dt_tr_sp.label == name_file]
    del pd3['label']
    # content_file = np.loadtxt(name_file, delimiter=';')
    content_file = pd3.to_numpy()
    list_group.append(content_file)
    list_mean.append(mean_class(content_file))
    cov_matrix_temp = cov_matrix(content_file)
    try:
        inv_matrix_temp = inv_matrix(cov_matrix_temp)
        list_inv_cov.append(inv_matrix_temp)
        del inv_matrix_temp
    except np.linalg.LinAlgError as err:
        print(err, ":", name_file)
        print("Value : \n", content_file)
        print("cov_matrix : \n", cov_matrix_temp)
    list_det_cov.append(det(cov_matrix_temp))
    list_cov.append(cov_matrix_temp)
    eigVal, eigVec = calc_eigen_val_vec(cov_matrix_temp)
    list_eigVal.append(eigVal.real)
    list_eigVec.append(eigVec.real)
    list_delta.append(np.amin(eigVal.real))
    list_avg_delta.append(np.average(eigVal.real))
    del content_file, cov_matrix_temp, eigVal, eigVec, name_file, pd3
end1 = t.time()
print("proses mean, eigVec, eigVal : " + str(end1 - start))

list_data_testing_name = dt_te_sp['label'].to_list()
del dt_te_sp['label']
list_data_testing = dt_te_sp.to_numpy()

accuracy_max = 0.0
np_list_group_name = np.asarray(group_name, dtype=np.str)
np_list_mean = np.asarray(list_mean, dtype=np.float64)
np_list_data_testing = np.asarray(list_data_testing, dtype=np.float64)
np_list_data_testing_name = np.asarray(list_data_testing_name, dtype=np.str)
np_list_eigVal = np.asarray(list_eigVal, dtype=np.float64)
np_list_eigVec = np.asarray(list_eigVec, dtype=np.float64)
np_list_delta = np.asarray(list_avg_delta, dtype=np.float64)

del list_group, list_det_cov, list_inv_cov, list_cov, dt_tr_sp,
del group_name, list_mean, list_data_testing, list_data_testing_name, list_eigVal, list_eigVec, ls_gn

MODE = "TRAINING 1"

start = t.time()
name_file_log = "MQDF2_" + MODE + "_" + str(dtt.datetime.now()) + ".txt"
# f = open(name_file_log, "w+")
if MODE == "TRAINING 1":
    print("start mqdf2... ", dtt.datetime.now())
    for val_k in range(1, 3):
        for h2 in np.arange(0.01, 0.02, 0.01):
            if accuracy_max != 100.0:
                get_val = mqdf2_7(np_list_group_name, np_list_mean, np_list_data_testing, np_list_data_testing_name,
                                  np_list_eigVal, np_list_eigVec, val_k, h2, accuracy_max, np_list_delta, p_col)
                accuracy_max = get_val[0]
                # f.writelines("\nMQDF2_7; k=" + str(val_k) + ";h2=" + str(round(h2,4)) + ";a=" + str(round(get_val[1],4))
                #             + "%;time=" + str(round(get_val[2],4)))
elif MODE == "TESTING 1":
    val_k = 1
    h2 = 0.6
    get_val = mqdf2_7(np_list_group_name, np_list_mean, np_list_data_testing, np_list_data_testing_name, np_list_eigVal,
                        np_list_eigVec, val_k, h2, accuracy_max)
    f.writelines("\nMQDF2_7; k=" + str(val_k) + ";h2=" + str(round(h2, 4)) + ";a=" + str(round(get_val[1], 4))
                 + "%;time=" + str(round(get_val[2], 4)))
elif MODE == "TESTING 2":
    for val_k in range(1, 4):
        for h2 in np.arange(0.01, 1, 0.01):
            get_val = mqdf2_7(np_list_group_name, np_list_mean, np_list_data_testing, np_list_data_testing_name,
                    np_list_eigVal, np_list_eigVec, val_k, h2, accuracy_max)
            f.writelines("\nMQDF2_7; k=" + str(val_k) + ";h2=" + str(round(h2, 4)) + ";a=" + str(round(get_val[1], 4))
                         + "%;time=" + str(round(get_val[2], 4)))
elif MODE == "TRAINING 2":
    print("start dlqdf... ", dtt.datetime.now())
    for val_k in range(1, p_col):
        for h2 in np.arange(0.01, 0.02, 0.01):
            if accuracy_max != 100.0:
                get_val = dlqdf25(np_list_group_name, np_list_mean, np_list_data_testing, np_list_data_testing_name,
                                  np_list_eigVal, np_list_eigVec, val_k, h2, accuracy_max, np_list_delta, p_col)
                accuracy_max = get_val[0]
            # f.writelines("\nMQDF2_7; k=" + str(val_k) + ";h2=" + str(round(h2, 4)) + ";a=" + str(round(get_val[1], 4))
            #              + "%;time=" + str(round(get_val[2], 4)))
end1 = t.time()
print("mqdf2 finish : " + str(end1 - start))
#f.close()
snapshot = tracemalloc.take_snapshot()
display_top(snapshot)

