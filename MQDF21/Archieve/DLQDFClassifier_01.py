import math as m
import time as t
import numpy as np

# faster but accurate low because only update class c and r althought all class should update paramater

class DLQDFClassifier_01:
    __slots__ = ['class_w', 'group_name', 'xi', 'alpha', 'n_slice', 'p_vec', 'j', 'is_print', 'lr1', 'lr2', 'lr3',
                 's_sigma_o', 's_tau_o', 's_mu_o', 's_sigma', 's_tau', 's_mu', 'iteration']

    def __init__(self, class_w, group_name, p_vec, n_slice):
        self.class_w = class_w
        self.group_name = group_name
        # self.all_k = None
        self.xi = None  # ξ
        self.alpha = None
        self.n_slice = n_slice # d / number of eigen vector
        self.p_vec = p_vec
        self.j = None
        self.is_print = False
        self.lr1 = None
        self.lr2 = None
        self.lr3 = None
        self.iteration = 0

        self.s_sigma_o = None
        self.s_tau_o = None
        self.s_mu_o = None
        self.s_sigma = None
        self.s_tau = None
        self.s_mu = None

    def set_xi_alpha(self, xi=0.0, alpha=0.0):
        self.xi = xi
        self.alpha = alpha

    def set_lr(self, lr1, lr2, lr3):
        self.lr1 = lr1
        self.lr2 = lr2
        self.lr3 = lr3
        
    def set_it(self, it):
        self.iteration = it

    def dlqdf_classifier(self, x_te, y_te, is_mqdf3=False, is_print=False ):
        p_class_w = len(self.class_w)  # i
        self.is_print = is_print
        p_x_te = len(x_te)  # j
        percent_print = p_x_te / 10
        self.s_sigma_o = np.array([[[None] * self.n_slice] * 2] * p_x_te, dtype=np.float32)
        self.s_tau_o = np.array([[[None] * 1] * 2] * p_x_te, dtype=np.float32)
        self.s_mu_o = np.array([[[None] * self.p_vec] * 2] * p_x_te, dtype=np.float32)
        self.s_sigma = np.array([[[None] * self.n_slice] * 2] * p_x_te, dtype=np.float32)
        self.s_tau = np.array([[[None] * 1] * 2] * p_x_te, dtype=np.float32)
        self.s_mu = np.array([[[None] * self.p_vec] * 2] * p_x_te, dtype=np.float32)
        hc_x = np.array([None] * p_x_te, dtype=np.float32)
        ind_c = np.array([[None]*2] * p_x_te)  # 0=c 1=r
        it = 0
        is_dlqdf = False
        bound_iteration = self.iteration
        print("xi=", self.xi, "alpha=", self.alpha, "lr1=", self.lr1, "lr2=", self.lr2, "lr3=", self.lr3)
        accuracy = 0.0
        while it < bound_iteration and accuracy != 100.00:
            start2 = t.time()
            count = 0
            l1 = 0.0
            if is_print:
                print("Iteration=", it)
            for j in range(p_x_te):
                self.j = j
                x = x_te[j]
                y = y_te[j][0]
                name_result = ""
                dq_temp = float("inf")
                dq_c = 0.0
                dq_r = float("inf")
                for i in range(p_class_w):
                    w = self.class_w[i]
                    # Note: more memory leak in here. please change into calc. no need array. Done
                    if is_dlqdf:
                        if i == ind_c[j][0]:
                            self.s_sigma_o[j][0] = rv_sigma[j][0]
                            self.s_tau_o[j][0] = rv_tau[j][0]
                            self.s_mu_o[j][0] = rv_mu[j][0]
                            w.eig_val = np.power(m.e, rv_sigma[j][0])
                            w.eig_val_min = np.power(m.e, rv_tau[j][0])
                            w.mean = rv_mu[j][0]
                        elif i == ind_c[j][1]:
                            self.s_sigma_o[j][1] = rv_sigma[j][1]
                            self.s_tau_o[j][1] = rv_tau[j][1]
                            self.s_mu_o[j][1] = rv_mu[j][1]
                            w.eig_val = np.power(m.e, rv_sigma[j][1])
                            w.eig_val_min = np.power(m.e, rv_tau[j][1])
                            w.mean = rv_mu[j][1]
                    # dq_13 = self.mqdf3_11_c(x, w)
                    # dq_13 = self.mqdf3_11_c(x, w) if is_mqdf3 else self.mqdf2_7_c(x, w)
                    if is_dlqdf is False: # lakukan sekali perhitungan mqdf
                        dq_13, s_tau_0, s_sigma_0, s_mu_0 = self.mqdf2_7_00(x, w, is_dlqdf)
                    elif is_dlqdf and ( ind_c[j][0] == i or ind_c[j][1] == i): # setelah update hanya menghitung c dan r
                        dq_13, s_tau_0, s_sigma_0, s_mu_0 = self.mqdf2_7_00(x, w, is_dlqdf)
                    if w.name == y:
                        dq_c = dq_13
                        ind_c[j][0] = i
                        self.s_tau[j][0] = s_tau_0
                        self.s_sigma[j][0] = s_sigma_0
                        self.s_mu[j][0] = s_mu_0
                        if is_dlqdf is False:
                            self.s_sigma_o[j][0] = w.eig_val
                            self.s_tau_o[j][0] = w.eig_val_min
                            self.s_mu_o[j][0] = w.mean
                    if dq_r > dq_13 and w.name != y:
                        dq_r = dq_13
                        ind_c[j][1] = i
                        self.s_tau[j][1] = s_tau_0
                        self.s_sigma[j][1] = s_sigma_0
                        self.s_mu[j][1] = s_mu_0
                        if is_dlqdf is False:
                            self.s_sigma_o[j][1] = w.eig_val
                            self.s_tau_o[j][1] = w.eig_val_min
                            self.s_mu_o[j][1] = w.mean
                    if dq_temp > dq_13:
                        name_result = w.name
                        dq_temp = dq_13
                if name_result == y:
                    count += 1
                hc = dq_c - dq_r
                lc_pow = -self.xi * hc
                if is_print:
                    print("id_te:", j, "it=",it, "c:", y, "=", round(dq_c, 2), ";r:", name_result, "=", round(dq_r, 2))
                lc = 1 / (1 + m.pow(m.e, lc_pow.real))
                hc_x[j] = lc
                l1 = l1 + lc.real + self.alpha * dq_c
                check_percent = j % percent_print
                if m.ceil(check_percent) == int(percent_print):
                    p_bar = int(j / percent_print * 10)
                    print("=", p_bar, "=", end="")
            # rv_sigma, rv_tau, rv_mu = self.update_sgd(s_sigma, s_tau, s_mu, s_sigma_o, s_tau_o, s_mu_o, p_x_te,
            #                                           p_class_w, ind_c, hc_x)
            rv_sigma, rv_tau, rv_mu = self.update_sgd(p_x_te, ind_c, hc_x)
            l1 = l1 / p_x_te
            accuracy = count / p_x_te * 100
            end2 = t.time()
            proses = end2 - start2
            title = "DLQDF_13-MQDF3_11" if is_mqdf3 else "DLQDF_13-MQDF2_7"
            print(title,"It=", it, "a=", round(accuracy, 4), "%;L1=", round(l1, 4), "; t=", round(proses, 2))
            is_dlqdf = True
            it += 1

    # def update_sgd(self, s_sigma, s_tau, s_mu, s_sigma_o, s_tau_o, s_mu_o, p_x_te, p_class_w, ind_c, hc_x):
    def update_sgd(self, p_x_te, ind_c, hc_x):
        rv_sigma = np.array([[[None] * self.n_slice] * 2] * p_x_te, dtype=np.float32)
        rv_tau = np.array([[[None] * 1] * 2] * p_x_te, dtype=np.float32)
        rv_mu = np.array([[[None] * self.p_vec] * 2] * p_x_te, dtype=np.float32)
        for j in range(p_x_te):
            for i in range(2):
                sigma_t = self.s_sigma_o[j][i]
                tau_t = self.s_tau_o[j][i]
                mu_t = self.s_mu_o[j][i]
                sigma_t0 = self.s_sigma[j][i]
                tau_t0 = self.s_tau[j][i]
                mu_t0 = self.s_mu[j][i]
                if i == 0:
                    # genuine class
                    sigma_t1 = sigma_t-self.lr1*((self.xi*hc_x[j]*(1-hc_x[j])*sigma_t0)+self.alpha*sigma_t0)
                    tau_t1 = tau_t-self.lr2*((self.xi*hc_x[j]*(1 - hc_x[j])*tau_t0)+self.alpha*tau_t0)
                    mu_t1 = mu_t - self.lr3*((self.xi*hc_x[j]*(1 - hc_x[j])*mu_t0)+self.alpha*mu_t0)
                else:
                    # rival class and other else
                    sigma_t1 = sigma_t-self.lr1*(self.xi * hc_x[j] * (1 - hc_x[j])*sigma_t0)
                    tau_t1 = tau_t-self.lr2 * (self.xi * hc_x[j] * (1 - hc_x[j])*tau_t0)
                    mu_t1 = mu_t-self.lr3 * (-1*self.xi * hc_x[j] * (1 - hc_x[j])*mu_t0)
                rv_sigma[j][i] = sigma_t1
                rv_tau[j][i] = tau_t1
                rv_mu[j][i] = mu_t1
        return rv_sigma, rv_tau, rv_mu

    # 5 - 16 Discriminative Learning Quadratic Discriminant Function for Handwriting Recognition -- liu2004
    # i = idx class w, j = idx testing, k = bound.
    def dlqdf2_5_c(self, x, w):
        x_s_m = x - w.mean
        norm = np.linalg.norm(x_s_m)
        r1 = norm ** 2
        k = w.k
        delta = np.power(w.eig_val_min, -1)
        eig_val = w.eig_val[:k]
        e_pow_min_log = np.power(m.e, np.log(eig_val) * -1.0)
        eig_vec = w.eig_vec[:, :k].real
        sum_1_1 = np.dot(np.transpose(eig_vec), x_s_m)
        sum_1_2 = np.power(sum_1_1, 2.0)
        sum_1_3 = np.dot(np.transpose(e_pow_min_log), sum_1_2)
        if isinstance(sum_1_3, np.ndarray):
            slice_sum_1_3 = sum_1_3[0:k]
        else:
            slice_sum_1_3 = sum_1_3
        sum_1 = np.sum(slice_sum_1_3)
        slice_sum_2_2 = sum_1_2[0:k]
        sum_2_2 = np.sum(slice_sum_2_2)
        sum_2 = np.power(m.e, np.log(delta) * -1) * (r1 - sum_2_2)
        sum_3_1 = np.log(eig_val)
        slice_sum_3_1 = sum_3_1[0:k]
        sum_3 = np.sum(slice_sum_3_1)
        sum_4 = (self.d - k) * m.log10(delta)
        dq_13 = sum_1 + sum_2 + sum_3 + sum_4
        if self.is_print:
            print("i: ", self.j, "; gn:", w.name, "; ", round(dq_13, 2), "= ", round(sum_1, 2), "+ ", round(sum_2, 2),
                  "+ ",
                  round(sum_3, 2), "+ ", round(sum_4, 2))
        return dq_13

    def mqdf3_11_c(self, x, w, j=0):
        x_s_m = x - w.mean
        norm = np.linalg.norm(x_s_m)
        r1 = norm ** 2
        k = w.k
        delta = w.eig_val_min
        eig_val = w.eig_val_int[:k]
        one_per = np.power(eig_val, -1.0)
        eig_vec = w.eig_vec_int[:, :k].real
        sum_1_1 = np.dot(np.transpose(eig_vec), x_s_m)
        sum_1_2 = np.power(sum_1_1, 2.0)
        sum_1_3 = np.dot(np.transpose(one_per), sum_1_2)
        if isinstance(sum_1_3, np.ndarray):
            slice_sum_1_3 = sum_1_3[0:k]
        else:
            slice_sum_1_3 = sum_1_3
        sum_1 = np.sum(slice_sum_1_3)
        slice_sum_2_2 = sum_1_2[0:k]
        sum_2_2 = np.sum(slice_sum_2_2)
        sum_2 = 1 / delta * (r1 - sum_2_2)
        sum_3_1 = np.log(eig_val)
        slice_sum_3_1 = sum_3_1[0:k]
        sum_3 = np.sum(slice_sum_3_1)
        sum_4 = (w.d - k) * m.log(delta)
        g2_7 = sum_1 + sum_2 + sum_3 + sum_4
        if self.is_print:
            print("i: ", self.j, "; gn:", w.name, "; ", round(g2_7, 2), "= ", round(sum_1, 2), "+ ", round(sum_2, 2),
                  "+ ",
                  round(sum_3, 2), "+ ", round(sum_4, 2))
        return g2_7

    def mqdf2_7_c(self, x, w):
        x_s_m = x - w.mean
        norm = np.linalg.norm(x_s_m)
        r1 = norm ** 2
        k = w.k
        # delta = np.power(w.eig_val_min, -1) if w.eig_val_min > 1 else np.power(w.eig_val_min, 1)
        delta = np.power(w.eig_val_min, 1) if w.eig_val_min > 1 else np.array([1])
        eig_val = w.eig_val[:k]
        one_per = np.where(eig_val > 1, np.power(eig_val, -1.0), np.power(1.0, 1.0))
        eig_vec = w.eig_vec[:, :k].real
        sum_1_1 = np.dot(np.transpose(eig_vec), x_s_m)
        sum_1_2 = np.power(sum_1_1, 2.0)
        sum_1_3 = np.dot(np.transpose(one_per), sum_1_2)
        if isinstance(sum_1_3, np.ndarray):
            slice_sum_1_3 = sum_1_3[0:k]
        else:
            slice_sum_1_3 = sum_1_3
        sum_1 = np.sum(slice_sum_1_3)
        slice_sum_2_2 = sum_1_2[0:k]
        sum_2_2 = np.sum(slice_sum_2_2)
        sum_2 = 1 / delta * (r1 - sum_2_2)
        sum_3_1 = np.log(eig_val)
        slice_sum_3_1 = np.abs(sum_3_1[0:k])
        sum_3 = np.sum(slice_sum_3_1)
        sum_4 = (w.d - k) * m.log(delta)
        g2_7 = sum_1 + sum_2 + sum_3 + sum_4  # no use sum_4 because to high minus
        if self.is_print:
            print("i: ", self.j, "; gn:", w.name, "; ", round(g2_7, 2), "= ", round(sum_1, 2), "+ ",
                  round(sum_2, 2), "+ ", round(sum_3, 2), "+ ", round(sum_4, 2), "eig_min:", delta)
        return g2_7

    def mqdf2_7_00(self, x, w, is_dlqdf=False):
        x_s_m = x - w.mean
        norm = np.linalg.norm(x_s_m)
        r1 = norm ** 2
        k = w.k
        delta = np.abs(w.eig_val_min)
        eig_val = np.abs(w.eig_val)
        lambda_ij = np.power(eig_val, -1.0) if is_dlqdf else np.power(eig_val, 1.0)
        # delta = np.power(w.eig_val_min, 1) if w.eig_val_min > 1 else np.array([1]) # make error
        # one_per = np.where(eig_val > 1, np.power(eig_val, -1.0), np.power(1.0, 1.0))
        eig_vec = w.eig_vec.real
        len_ij = x_s_m.shape
        x_s_m = x_s_m.reshape(len_ij[0],1)
        p_ij = np.dot(eig_vec, x_s_m)
        p2_ij = np.power(p_ij, 2.0)
        sum_1_3 = np.dot(np.transpose(lambda_ij), p2_ij)
        if isinstance(sum_1_3, np.ndarray):
            slice_sum_1_3 = sum_1_3[0:k]
        else:
            slice_sum_1_3 = sum_1_3
        sum_1 = np.sum(slice_sum_1_3)
        slice_sum_2_2 = p2_ij[0:k]
        sum_2_2 = np.sum(slice_sum_2_2)
        sum_2 = 1 / delta * (r1 - sum_2_2)
        sum_3_1 = np.log(eig_val)
        slice_sum_3_1 = np.abs(sum_3_1[0:k])
        sum_3 = np.sum(slice_sum_3_1)
        sum_4 = (self.n_slice - k) * m.log(delta)
        sum_2 = sum_2[0] if isinstance(sum_2, np.ndarray) else sum_2
        sum_4 = sum_4[0] if isinstance(sum_4, np.ndarray) else sum_4
        g2_7 = sum_1 + sum_2 + sum_3 + sum_4
        # partial derivative
        s_sigma_1 = np.sign(-m.e) * (np.abs(m.e) ** (-lambda_ij)) * p2_ij
        s_sigma_0 = s_sigma_1.diagonal() + 1
        my_d_k = (self.n_slice - k)
        s_tau_1 = np.sign(-m.e)*(np.abs(m.e)**(-delta))*(r1-sum_2_2)
        s_tau_0 = s_tau_1 + my_d_k
        s_mu_1_1 = delta - lambda_ij[:k]
        s_mu_1_2 = s_mu_1_1.reshape(k, 1).dot(p_ij[:k].reshape(1, k)).diagonal()
        s_mu_1_3 = s_mu_1_2.reshape(1, k).dot(eig_vec[:k]).reshape(self.p_vec, 1)
        s_mu_2 = 2 * delta * x_s_m
        s_mu_0 = 2 * s_mu_1_3 - s_mu_2
        # if self.is_print:
        #     print("i: ", self.j, "; gn:", w.name, "; ", round(g2_7, 2), "= ", round(sum_1, 2), "+ ",
        #           round(sum_2, 2), "+ ", round(sum_3, 2), "+ ", round(sum_4, 2), "eig_min:", round(delta))
        return g2_7, s_tau_0, s_sigma_0, s_mu_0.reshape(self.p_vec)
