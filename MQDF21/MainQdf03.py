import mqdf.MQDF3Classifier
import main_dlqdf_01 as md01


class MainQdf03:
    def __init__(self, index=0):
        self.index = index
        self.f_nm_pickle_mqdf = "/home/teguhteja/PycharmProjects/Tesis/MQDF21/w_1984_2384_0_170_0_1.0_.pickle"
        self.f_nm_pickle_dlqdf = "/home/teguhteja/PycharmProjects/Tesis/MQDF21/w_1984_2384_50_45_0_0.0_.pickle"

    def set_index(self, index):
        self.index = index

    def get_y_mqdf(self):
        save_dump = md01.m_load_dump(self.f_nm_pickle_mqdf)
        my_class_w_tr = save_dump[0]
        my_group_name_tr = save_dump[1]
        average_var_class = save_dump[2]
        te_x = save_dump[3]
        te_y = save_dump[4]
        k = save_dump[5]
        n_slice_eig_vec = save_dump[6]
        d = save_dump[7]

        x = [te_x[self.index]]
        y = [te_y[self.index]]

        name_result = md01.my_mqdf2_classifier(my_class_w_tr, my_group_name_tr, x, y, k, n_slice_eig_vec, d)
        return name_result

    def get_y_dlqdf(self):
        save_dump = md01.m_load_dump(self.f_nm_pickle_dlqdf)
        my_class_w_tr = save_dump[0]
        my_group_name_tr = save_dump[1]
        average_var_class = save_dump[2]
        te_x = save_dump[3]
        te_y = save_dump[4]
        k = save_dump[5]
        n_slice_eig_vec = save_dump[6]
        d = save_dump[7]

        xi = 0.0001
        alpha = 0.2
        lr = 0.1
        my_iter = 20

        x = [te_x[self.index]]
        y = [te_y[self.index]]

        name_result = md01.my_dlqdf_classifier(my_class_w_tr, my_group_name_tr, average_var_class, x, y, xi=xi,
                                               alpha=alpha, lr1=lr, lr2=lr, lr3=lr, d=d, n_slice=n_slice_eig_vec,
                                               iteration=my_iter)
        return name_result

