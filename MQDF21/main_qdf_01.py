import numpy as np
import time as t
import datetime as dtt
import mqdf.SelectColAndRetNp as Scarn
import mqdf.ClassW as Mcw
import mqdf.QDFClassifier as Mqdfc
import mqdf.CrossValidation as CrVa
from main_dlqdf_01 import home


def main_proses():
    print("start import :", dtt.datetime.now())
    start = t.time()
    x_tr_file = home+"/Tesis/MQDF21/data/X_train.csv"
    x_te_file = home+"/Tesis/MQDF21/data/X_test.csv"
    y_tr_file = home+"/Tesis/MQDF21/data/Y_train.csv"
    y_te_file = home+"/Tesis/MQDF21/data/Y_test.csv"
    st_idx = 1984
    en_idx = 2384
    my_xy_tr = Scarn(x_tr_file, y_tr_file, st_idx, en_idx)
    my_xy_tr.process()
    my_xy_tr.distinct_group_name()
    my_te = Scarn(x_te_file, y_te_file, st_idx, en_idx)
    my_te.process_te()
    my_group_name_tr = my_xy_tr.get_group_name()
    end1 = t.time()
    print("import file : " + str(end1 - start))

    print("start Cross Validation :", dtt.datetime.now())
    start = t.time()
    my_croval = CrVa(my_xy_tr.get_xy_sel(), my_te.get_x(), my_te.get_y(),my_group_name_tr)
    my_croval.sub_sampling(1.0, False)
    np_xy_tr = my_croval.get_xy_cv()
    end1 = t.time()
    print("proses Cross Validation  : " + str(end1 - start))

    # np_xy_tr = my_xy_tr.get_xy_sel()
    print("start preprocess :", dtt.datetime.now())
    start = t.time()
    class_w_tr = Mcw(np_xy_tr, my_group_name_tr)
    # class_w_tr.distinct_group_name()
    class_w_tr.deplet_xy()
    my_class_w_tr = class_w_tr.get_class_w()
    end1 = t.time()
    print("proses mean, eigVec, eigVal : " + str(end1 - start))

    print("start classifier :", dtt.datetime.now())
    start = t.time()
    mqdfc = Mqdfc(my_class_w_tr, my_group_name_tr)
    mqdfc.mqdf_classifier(my_te.get_x(), my_te.get_y(), d=300, mode_loop=False)
    end1 = t.time()
    print("classifier : " + str(end1 - start))

if __name__ == "__main__":
    main_proses()
