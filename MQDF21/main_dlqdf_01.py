import time as t
import datetime as dtt
# from guppy import hpy
# import pdb
# import mqdf
import mqdf.SelectColAndRetNp as Scarn
import mqdf.ClassW as Mcw
import mqdf.MQDF3Classifier as Mqdf3c
import mqdf.DLQDFClassifier as Dlqdf
import mqdf.CrossValidation as CrVa
import mqdf.SmallTesting as St
import mqdf.QDFClassifier as Mqdf2
import pickle as pck
import os.path as opa
# import CIC as cic
import argparse
import os
from mqdf import SplitData
from mqdf import SelectColFromFeatureSelection as Scffs
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import f_classif
from sklearn.feature_selection import chi2
import logging
from pympler import asizeof
import sys

# print pid
os.getpid()
home = "/home/ttm/P/PythonProjects"
logging.basicConfig(filename=f'log/app_{dtt.datetime.now()}.log', filemode='w', format='%(message)s')


# for dlqdf use mqdf2_7
# use training data as train and testing
def log_process(status=None, name_function=None, g_start=None, msg=None):
    if status == 'START':
        msg = f"start {name_function} : {dtt.datetime.now()}"
        print(msg)
        logging.warning(msg)
        g_start = t.time()
        return g_start
    elif status == 'END':
        end1 = t.time()
        msg = f"{name_function} : {round((end1 - g_start), 2)} s"
        print(msg)
        logging.warning(msg)
    else:
        print(msg)
        logging.warning(msg)


def process_import(st_idx, en_idx, is_mode_training=False):
    g_start = log_process('START', 'import')
    x_tr_file = home + "/Tesis/MQDF21/data/X_train.csv"
    x_te_file = home + "/Tesis/MQDF21/data/X_test.csv"
    y_tr_file = home + "/Tesis/MQDF21/data/Y_train.csv"
    y_te_file = home + "/Tesis/MQDF21/data/Y_test.csv"
    my_xy_tr = Scarn(x_tr_file, y_tr_file, st_idx, en_idx, log_process)
    my_xy_tr.process()
    my_xy_tr.distinct_group_name()
    x_te = None
    y_te = None
    if is_mode_training:
        x_te = x_tr_file
        y_te = y_tr_file
    else:
        x_te = x_te_file
        y_te = y_te_file
    my_te = Scarn(x_te, y_te, st_idx, en_idx, log_process)
    my_te.process_te()
    my_group_name_tr = my_xy_tr.get_group_name()
    log_process('END', 'import file', g_start=g_start)
    return my_xy_tr, my_te, my_group_name_tr


def cross_validation(ratio, bool_val, my_xy_tr, x, y, my_group_name_tr):
    print("start Cross Validation :", dtt.datetime.now())
    start = t.time()
    my_cro_val = CrVa(my_xy_tr.get_xy_sel(), x, y, my_group_name_tr)
    my_cro_val.sub_sampling(ratio, bool_val)
    np_xy_tr = my_cro_val.get_xy_cv()
    end1 = t.time()
    print("proses Cross Validation  : ", round((end1 - start), 2), "s")
    return np_xy_tr


def preprocessed(gamma_val, np_xy_tr, my_group_name_tr, n_round, is_gso=False, n_slice_eig_vec=None, d=0, all_k=None,
                 min_eig_val_i=None, is_mqdf3=False, is_print=False):
    g_start = log_process('START', 'Preprocessed')
    class_w_tr = Mcw(np_xy_tr, my_group_name_tr, log_process, is_print)
    class_w_tr.set_gamma(gamma_val)
    class_w_tr.set_all_k(all_k)
    class_w_tr.set_n_slice_eig_vec(n_slice_eig_vec)
    class_w_tr.set_n_round(n_round)
    class_w_tr.set_d(d)
    class_w_tr.set_minor_eig_val_all(min_eig_val_i)
    class_w_tr.deplet_xy(is_gso, is_mqdf3)
    my_class_w_tr = class_w_tr.get_class_w()
    log_process('END', 'proses mean, eigVec, eigVal', g_start=g_start)
    return my_class_w_tr, class_w_tr.get_avr_var()


def g_s_testing(pd_xy_tr, ratio, my_group_name_tr, d, is_random=False, fixed_sample=0, _x=None, _y=None):
    print("start Small Testing :", dtt.datetime.now())
    start = t.time()
    my_st = St(pd_xy_tr, d, my_group_name_tr, ratio, is_random, fixed_sample, _x, _y)
    my_st.process_small_testing()
    end1 = t.time()
    print("proses get small testing : ", round((end1 - start), 2), "s")
    return my_st.get_xy()


def my_dlqdf_classifier(my_class_w_tr, my_group_name_tr, average_var_class, x, y, is_bounded,
                        xi=0.01, alpha=0.2, lr1=0.0001, lr2=0.0001, lr3=0.0001, d=400, n_slice=0, iteration=20,
                        is_print=False, minor_eig_val=None, all_k=None):
    g_start = log_process('START', 'dlqdf classifier')
    log_process(msg=f'class w size : {asizeof.asizeof(my_class_w_tr)}')
    dlqdf = Dlqdf(my_class_w_tr, my_group_name_tr, d, n_slice, log_process, is_bounded)
    dlqdf.set_xi_alpha(xi, alpha)
    dlqdf.set_lr(lr1, lr2 * average_var_class, lr3)
    dlqdf.set_it(iteration)
    dlqdf.set_mev(minor_eig_val)
    dlqdf.set_all_k(all_k)
    dlqdf.set_print(is_print)
    del my_class_w_tr, my_group_name_tr
    name_result = dlqdf.dlqdf_classifier(x, y)
    log_process('END', 'classifier dlqdf', g_start=g_start)
    # return name_result


def my_mqdf_classifier(my_class_w_tr, my_group_name_tr, x, y, d, n_slice_eig_vec=None, min_eig_val_i=None, my_cic=None,
                       all_k=None, is_print=False, is_mqdf3=False):
    g_start = log_process('START', 'mqdf classifier')
    mqdf3c = Mqdf3c(my_class_w_tr, my_group_name_tr, n_slice_eig_vec, d, log_process)
    mqdf3c.set_all_k(all_k)
    mqdf3c.set_all_delta(min_eig_val_i)
    mqdf3c.set_cic_delta(my_cic)
    mqdf3c.set_is_print(is_print)
    mqdf3c.mqdf2_classifier(x, y, is_mqdf3=is_mqdf3)
    name_result = mqdf3c.get_name_result()
    # m_save_dump("conf_matrix.pickle", mqdf3c.get_conf_matrix())
    log_process('END', 'classifier mqdf', g_start=g_start)
    return name_result


def my_mqdf2_classifier(my_class_w_tr, my_group_name_tr, x, y, k, n_slice_eig_vec, d):
    print("start mqdf classifier :", dtt.datetime.now())
    start = t.time()
    mqdf2 = Mqdf2(my_class_w_tr, my_group_name_tr)
    # mqdf2.set_all_k(k)
    name_result = mqdf2.mqdf_classifier(x, y, k, mode_loop=False)
    end1 = t.time()
    print("classifier mqdf : ", round((end1 - start), 2), "s")
    return name_result


def get_name_file(st=0, en=0, index=0, gamma=0.0, n_slice=0, k=0, tipe_file="", mode=""):
    nm = ""
    if mode == 1:
        nm = "i_" + str(st) + "_" + str(en) + "_" + tipe_file + ".pickle"
    elif mode == 2:
        nm = "w_" + str(index) + "_" + str(gamma) + "_" + str(n_slice) + "_" + str(k) + ".pickle"
    return nm


def get_file_exist(nm_file):
    if opa.isfile(nm_file):
        return True
    else:
        return False


def m_save_dump(nm_file, save_dump):
    print("save variable to :", nm_file)
    with open(nm_file, "wb") as f:
        pck.dump(save_dump, f)


def m_load_dump(nm_file):
    print("load variable from :", nm_file)
    save_dump = None
    with open(nm_file, "rb") as f:
        save_dump = pck.load(f)
    return save_dump


def my_arrange(st, it, depth):
    rv = [0.0] * it
    for i in range(it):
        rv[i] = st * 10 ** (depth + 1) + (i) * 10 ** (depth - 1)
    return rv


def get_range(it, max_k):
    a = max_k / it
    rv = [None] * (it ** 2)
    for i in range(it):
        slice = int(a * (i + 1))
        for j in range(it):
            k = int(slice / 4) * (j + 1)
            idx = it * i + j
            rv[idx] = [k, slice]
    return rv


def get_st_en_idx(i_mode):
    a_st = [0, 0, 1984, 2385, 0, 0]
    a_en = [2589, 1983, 2384, 2589, 4, 7]
    st_idx = a_st[i_mode]
    en_idx = a_en[i_mode]
    d = en_idx - st_idx
    return st_idx, en_idx, d


def my_arg_parse(index, is_mode_training, n_slice_eig_vec, n_k, min_eig_val, gamma, is_gso, is_mqdf3, is_mode_mqdf,
                 is_s_te, ratio_sm, is_small_testing_random, fixed_sample, iteration, is_trace_memory, xi, alpha, lr,
                 is_print):
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--index", help="select type 0.All 1.HOG 2.NPWK 3.Zoning ")
    parser.add_argument("-t", "--is_mode_training", help="if true use data training else use data testing")
    parser.add_argument("-n", "--n_slice_eig_vec", help="set number slice eigen vector")
    parser.add_argument("-k", "--n_k", help="set number k")
    parser.add_argument("-v", "--min_eig_val", help="set minimal eigen value")
    parser.add_argument("-g", "--gamma", help="set gamma for MQDF3")
    parser.add_argument("-o", "--is_gso", help="Calc Eigen Vector use GSO")
    parser.add_argument("-m", "--is_mode_mqdf", help="if true use MQDF else DLQDF. By default MQDF ")
    parser.add_argument("-3", "--is_mqdf3", help="if true use MQDF3 else MQDF2")
    parser.add_argument("-c", "--is_s_te", help="if true then small testing")
    parser.add_argument("-r", "--ratio_sm", help="set ratio for small testing")
    parser.add_argument("-s", "--is_small_testing_random", help="if true small testing with random")
    parser.add_argument("-f", "--fixed_sample", help="set number fixed sample")
    parser.add_argument("-i", "--iteration", help="iteration for DLQDF")
    parser.add_argument("-e", "--is_trace_memory", help="if true then trace memory. use leftover")
    parser.add_argument("-x", "--xi", help="set xi ratio")
    parser.add_argument("-a", "--alpha", help="set value alpha")
    parser.add_argument("-l", "--lr", help="set learning for lr1, lr2, lr3")
    parser.add_argument("-p", "--is_print", help="verbose method mqdf or dlqdf")
    args = parser.parse_args()
    if args.index:
        index = int(args.index)
    if args.is_mode_training:
        is_mode_training = args.is_mode_training.lower() == "true"
    if args.n_slice_eig_vec:
        n_slice_eig_vec = None if args.n_slice_eig_vec.lower() == 'None' else int(args.n_slice_eig_vec)
    if args.n_k:
        n_k = None if args.n_k.lower() == 'None' else int(args.n_k)
    if args.min_eig_val:
        min_eig_val = None if args.min_eig_val.lower() == 'None' else float(args.min_eig_val)
    if args.gamma:
        gamma = float(args.gamma)
    if args.is_gso:
        is_gso = args.is_gso.lower() == 'true'
    if args.is_mqdf3:
        is_mqdf3 = args.is_mqdf3.lower() == 'true'
    if args.is_mode_mqdf:
        is_mode_mqdf = args.is_mode_mqdf.lower() == 'true'
    if args.is_s_te:
        is_s_te = args.is_s_te.lower() == 'true'
    if args.ratio_sm:
        ratio_sm = float(args.ratio_sm)
    if args.is_small_testing_random:
        is_small_testing_random = args.is_small_testing_random.lower() == 'true'
    if args.fixed_sample:
        fixed_sample = int(args.fixed_sample)
    if args.iteration:
        iteration = int(args.iteration)
    if args.is_trace_memory:
        is_trace_memory = args.is_trace_memory.lower() == 'true'
    if args.xi:
        xi = float(args.xi)
    if args.alpha:
        alpha = float(args.alpha)
    if args.lr:
        lr = float(args.lr)
    if args.is_print:
        is_print = args.is_print.lower() == 'true'
    return index, is_mode_training, n_slice_eig_vec, n_k, min_eig_val, gamma, is_gso, is_mqdf3, is_mode_mqdf, is_s_te, ratio_sm, is_small_testing_random, fixed_sample, iteration, is_trace_memory, xi, alpha, lr, is_print


def split_dt_tr(number, my_xy_tr, tr_x, tr_y, my_group_name_tr):
    g_start = log_process('START', 'Split Data Training')
    my_sp_dt = SplitData(my_xy_tr.get_xy_sel(), tr_x, tr_y, my_group_name_tr)
    spdt_xy_tr, spdt_tr_x, spdt_tr_y = my_sp_dt.process(number)
    log_process(msg=f"Split Data for {number}")
    log_process('END', 'Split Data Training', g_start=g_start)
    return spdt_xy_tr, spdt_tr_x, spdt_tr_y


def process_feature_selection(tr_x, tr_y, te_x, te_y, my_tr_xy, is_fs_in_te, num_feature_sel=50):
    g_start = log_process('START', 'Feature Selection')
    new_tr_x = SelectKBest(chi2, k=num_feature_sel).fit_transform(tr_x, tr_y)
    new_te_x = SelectKBest(chi2, k=num_feature_sel).fit_transform(te_x, te_y)
    te_x = None if is_fs_in_te is False else te_x
    scffs = Scffs(new_tr_x, tr_y, my_tr_xy, num_feature_sel, te_x)
    te_x = new_te_x if is_fs_in_te is False else te_x
    msg = f"Feature Selection for {num_feature_sel} field(s)"
    scffs.get_new_xy()
    log_process(msg=msg)
    log_process('END', 'Feature Selection', g_start=g_start)
    return new_tr_x, te_x, scffs


def process_mqdf_dlqdf(is_mode_mqdf, my_class_w_tr, my_group_name_tr, te_x, te_y, d, n_slice_eig_vec, min_eig_val_i,
                       is_print, is_mqdf3, k, average_var_class, xi, alpha, lr, iteration, is_bounded):
    if is_mode_mqdf:
        my_mqdf_classifier(my_class_w_tr, my_group_name_tr, te_x, te_y, d, n_slice_eig_vec=n_slice_eig_vec,
                           min_eig_val_i=min_eig_val_i, my_cic=None, is_print=is_print, is_mqdf3=is_mqdf3, all_k=k)
    else:
        my_dlqdf_classifier(my_class_w_tr, my_group_name_tr, average_var_class, te_x, te_y, is_bounded, xi=xi,
                            alpha=alpha, lr1=lr, lr2=lr, lr3=lr, d=d, n_slice=n_slice_eig_vec, iteration=iteration,
                            is_print=is_print,
                            all_k=k)


def main_proses_cv():
    index = 3  # 0.All 1.HOG 2.NPW-Kirch 3. Zoning
    n_slice_eig_vec = None
    k = 50
    # req_k = [20, 40, 60, 80, 100, 120]
    # req_k = [40, 80, 120, 160, 200, 240]
    req_k = [160, 200, 240]
    # number_feature = 204
    number_feature = 400

    min_eig_val_i = None
    gamma = 0.1
    is_gso = True
    is_mode_mqdf = False  # False is DLQDF
    is_mqdf3 = False
    ratio_sm = 0.6
    fixed_sample = 0
    is_trace_memory = False
    iteration = 10
    xi = 1e-16
    alpha = 0.2
    lr = 0.1
    is_print = False
    # default value
    is_mode_training_cv = False
    is_mode_training = True
    is_bounded_dlqdf = True
    is_fs_in_te = False
    is_s_te = False
    is_small_testing_random = True
    number_split = 1
    # min_eig_val_i = 5.0e-4  # if none then calc mev else set all mev same number
    min_eig_val_i = None
    # min_eig_val_i = 4  # for dlqdf

    for my_index in range(1, 0, -1):
        st_idx, en_idx, d = get_st_en_idx(my_index)  # 0.All 1.HOG 2.NPW-Kirch 3.Zoning
        d = number_feature if number_feature != 0 else d
        # if is_trace_memory:
        #     hp = hpy()
        #     before = hp.heap()
        # import process
        my_xy_tr, my_te, my_group_name_tr = process_import(st_idx, en_idx, is_mode_training=is_mode_training)
        tr_x = my_xy_tr.get_x()
        tr_y = my_xy_tr.get_y()
        te_x = my_te.get_x()
        te_y = my_te.get_y()

        # feature selection
        tr_x, te_x, scffs = process_feature_selection(tr_x, tr_y, te_x, te_y, my_xy_tr, is_fs_in_te, number_feature)
        # te_x = te_x if is_mode_training else scffs.get_te_x() comment for testing

        split_dt_xy_tr = []
        split_dt_tr_x = []
        split_dt_tr_y = []
        if number_split == 1:
            split_dt_xy_tr.append(scffs.get_xy_sel())
            split_dt_tr_x.append(tr_x)
            split_dt_tr_y.append(tr_y)
        else:
            split_dt_xy_tr, split_dt_tr_x, split_dt_tr_y = split_dt_tr(number_split, scffs, tr_x, tr_y,
                                                                       my_group_name_tr)
        l_class_w_tr = []
        l_avg_var_class = []

        if is_s_te:
            te_x, te_y = g_s_testing(split_dt_xy_tr[0], ratio_sm, my_group_name_tr, d,
                                     is_random=is_small_testing_random,
                                     fixed_sample=fixed_sample, _x=te_x, _y=te_y)

        for my_k in req_k:
            n_slice_eig_vec = my_k
            my_class_w_tr, average_var_class = preprocessed(gamma, split_dt_xy_tr[0], my_group_name_tr, 0,
                                                            is_gso=is_gso, d=d, is_mqdf3=True,
                                                            n_slice_eig_vec=n_slice_eig_vec, all_k=my_k)
            l_class_w_tr.append(my_class_w_tr)
            l_avg_var_class.append(average_var_class)

            s_title = "data Training" if is_mode_training else "data Testing"
            s_title = "MQDF " + s_title if is_mode_mqdf else "DLQDF " + s_title
            s_title = s_title + " K-Fold Cross Validation : " + str(number_split)
            # print(s_title)
            log_process(msg=s_title)

            process_mqdf_dlqdf(is_mode_mqdf, my_class_w_tr, my_group_name_tr, te_x, te_y, d, n_slice_eig_vec,
                               min_eig_val_i, is_print, is_mqdf3, my_k, average_var_class, xi, alpha, lr, iteration,
                               is_bounded_dlqdf)

    # if is_mode_training_cv:
    #     for i in range(number_split):
    #         my_class_w_tr = l_class_w_tr[i]
    #         average_var_class = l_avg_var_class[i]
    #         for j in range(number_split):
    #             if i != j or number_split == 1:
    #                 te_x = split_dt_tr_x[j]
    #                 te_y = split_dt_tr_y[j]
    #                 msg = f"data training {str(i + 1)} & data tested {str(j + 1)}"
    #                 # print(msg)
    #                 log_process(msg=msg)
    #                 process_mqdf_dlqdf(is_mode_mqdf, my_class_w_tr, my_group_name_tr, te_x, te_y, d, n_slice_eig_vec, min_eig_val_i,
    #                                    is_print, is_mqdf3, k, average_var_class, xi, alpha, lr, iteration)
    # else:
    #     process_mqdf_dlqdf(is_mode_mqdf, my_class_w_tr, my_group_name_tr, te_x, te_y, d, n_slice_eig_vec, min_eig_val_i,
    #                        is_print, is_mqdf3, k, average_var_class, xi, alpha, lr, iteration, is_bounded_dlqdf)
    # if is_trace_memory:
    #     after = hp.heap()
    #     leftover = after - before
    #     pdb.set_trace()


def main_proses():
    index = 3  # 0.All 1.HOG 1.NPW-Kirch 2. Zoning
    is_mode_training = True  # if true use data training else use data testing
    n_slice_eig_vec = 50
    k = None
    # ratio_cv = 0.0
    # n_round = 0
    # min_eig_val_i = 5.0e-4  # if none then calc mev else set all mev same number
    min_eig_val_i = None
    # min_eig_val_i = 4  # for dlqdf
    gamma = 0.1
    is_gso = False
    is_mode_mqdf = False  # False is DLQDF
    is_mqdf3 = True

    is_s_te = False  # small test with training data
    is_small_testing_random = False
    ratio_sm = 0.0
    fixed_sample = 60

    is_trace_memory = False

    iteration = 2
    xi = 1e-16
    alpha = 0.2
    lr = 0.1
    is_print = False

    index, is_mode_training, n_slice_eig_vec, k, min_eig_val_i, gamma, is_gso, is_mqdf3, is_mode_mqdf, is_s_te, ratio_sm, is_small_testing_random, fixed_sample, iteration, is_trace_memory, xi, alpha, lr, is_print = my_arg_parse(
        index, is_mode_training, n_slice_eig_vec, k, min_eig_val_i, gamma, is_gso, is_mqdf3, is_mode_mqdf, is_s_te,
        ratio_sm, is_small_testing_random, fixed_sample, iteration, is_trace_memory, xi, alpha, lr, is_print)

    type_file = "tr_tr" if is_mode_training else "tr_te"
    st_idx, en_idx, d = get_st_en_idx(index)  # 0.All 1.HOG 2.NPW-Kirch 3.Zoning
    nm_fl_i = get_name_file(st_idx, en_idx, index, gamma, n_slice=n_slice_eig_vec, k=k, tipe_file=type_file, mode=1)
    is_file_i = get_file_exist(nm_fl_i)
    # if is_trace_memory:
    #     hp = hpy()
    #     before = hp.heap()

    if is_file_i is False:
        my_xy_tr, my_te, my_group_name_tr = process_import(st_idx, en_idx, is_mode_training=is_mode_training)
        te_x = my_te.get_x()
        te_y = my_te.get_y()
        save_dump = [my_xy_tr, my_group_name_tr, te_x, te_y, d]
        m_save_dump(nm_fl_i, save_dump)
    else:
        save_dump = m_load_dump(nm_fl_i)
        my_xy_tr = save_dump[0]
        my_group_name_tr = save_dump[1]
        te_x = save_dump[2]
        te_y = save_dump[3]
        d = save_dump[4]
        del save_dump

    nm_fl_w = get_name_file(st_idx, en_idx, index, gamma, n_slice=n_slice_eig_vec, k=k, tipe_file=type_file, mode=2)
    # is_file_w = get_file_exist(nm_fl_w)
    is_file_w = False

    if is_file_w is False:
        pd_xy_tr = cross_validation(0.0, False, my_xy_tr, te_x, te_y, my_group_name_tr)
        my_class_w_tr, average_var_class = preprocessed(gamma, pd_xy_tr, my_group_name_tr, 0, is_gso=is_gso,
                                                        d=d, is_mqdf3=True, n_slice_eig_vec=n_slice_eig_vec,
                                                        all_k=k)
        save_dump = [my_class_w_tr, average_var_class, k, n_slice_eig_vec, pd_xy_tr]
        m_save_dump(nm_fl_w, save_dump)
    else:
        save_dump = m_load_dump(nm_fl_w)
        my_class_w_tr = save_dump[0]
        average_var_class = save_dump[1]
        k = save_dump[2]
        n_slice_eig_vec = save_dump[3]
        pd_xy_tr = save_dump[4]
        del save_dump

    s_title = "data Training" if is_mode_training else "data Testing"
    s_title = "MQDF " + s_title if is_mode_mqdf else "DLQDF " + s_title
    print(s_title)

    if is_s_te:
        te_x, te_y = g_s_testing(pd_xy_tr, ratio_sm, my_group_name_tr, d, is_random=is_small_testing_random,
                                 fixed_sample=fixed_sample, _x=te_x, _y=te_y)
        del pd_xy_tr

    if is_mode_mqdf:
        # for rv_k in get_range(4,400):
        #     k, n_slice_eig_vec = rv_k[0], rv_k[1]
        #     # if n_slice_eig_vec != 100 :
        # my_cic = cic.import_mev_csv()
        my_mqdf_classifier(my_class_w_tr, my_group_name_tr, te_x, te_y, d, n_slice_eig_vec=n_slice_eig_vec,
                           min_eig_val_i=min_eig_val_i, my_cic=None, is_print=is_print, is_mqdf3=is_mqdf3, all_k=k)
        # pass
    else:
        del my_xy_tr
        my_dlqdf_classifier(my_class_w_tr, my_group_name_tr, average_var_class, te_x, te_y, xi=xi, alpha=alpha, lr1=lr,
                            lr2=lr, lr3=lr, d=d, n_slice=n_slice_eig_vec, iteration=iteration, is_print=is_print,
                            all_k=k)

    # if is_trace_memory:
    #     after = hp.heap()
    #     leftover = after - before
    #     pdb.set_trace()


if __name__ == "__main__":
    main_proses_cv()
