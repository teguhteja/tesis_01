import tkinter
from tkinter import *
from tkinter.filedialog import askopenfilename
import cv2
import PIL.Image, PIL.ImageTk
import os
import numpy as np
import pandas as pd
import MainQdf03 as Mq03


class App:
    def __init__(self, window, window_title):
        self.window = window
        self.window.title(window_title)
        self.window.geometry("400x200")

        self.panelA = None
        self.panelB = None
        self.l_fl_nm = None
        self.idx = None
        self.ori_text = None
        self.is_dlqdf = True
        self.is_mqdf = True

        frame = tkinter.Frame(self.window)
        frame.pack(side=tkinter.BOTTOM)

        self.btn_open = tkinter.Button(frame, text="Open", command=self.open_dialog)
        self.btn_open.pack(side=tkinter.LEFT, fill="both", expand=True, padx="10", pady="10")

        self.btn_mqdf = tkinter.Button(frame, text="MQDF", command=self.exec_mqdf)
        self.btn_mqdf.pack(side=tkinter.LEFT, fill="both", expand=True, padx="10", pady="10")

        self.btn_dlqdf = tkinter.Button(frame, text="DLQDF", command=self.exec_dlqdf)
        self.btn_dlqdf.pack(side=tkinter.RIGHT, fill="both", expand=True, padx="10", pady="10")

        self.generate_gt_test_image_random("data/GT_test_image_random.txt")

        self.window.mainloop()

    def set_image(self, image_path):
        scale_inc = 4
        image = cv2.imread(image_path)
        width = int(image.shape[1] * scale_inc)
        height = int(image.shape[0] * scale_inc)
        image = cv2.resize(image, (width, height))
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        image = PIL.Image.fromarray(image)
        image = PIL.ImageTk.PhotoImage(image)

        if self.panelA is None:
            self.panelA = Label(image=image)
            self.panelA.image = image
            self.panelA.pack(side=tkinter.LEFT, padx=10, pady=10)
        else:
            self.panelA.configure(image=image)
            self.panelA.image = image

        text = self.gen_text(image_path)
        self.ori_text = text
        if self.panelB is None:
            self.panelB = Label(text=text, font=("Helvetica", 12), wraplength=240, justify="left", anchor="n")
            self.panelB.pack(side=tkinter.RIGHT, padx=10, pady=10)
        else:
            self.panelB.config(text=text)
        self.is_mqdf = True
        self.is_dlqdf = True

    # Callback for the "Blur" button
    def open_dialog(self):
        init_dir = "/home/teguhteja/Documents/ICHR_2016/Challenge-3-ForTest/test_image_random/"
        filename = askopenfilename(initialdir=init_dir, filetypes=(("JPG File", "*.jpg"), ("All Files", "*.*")))
        self.set_image(filename)

    def exec_mqdf(self):
        if self.panelB is not None and self.is_mqdf:
            my_mq03 = Mq03.MainQdf03()
            my_mq03.set_index(self.idx)
            name_result = my_mq03.get_y_mqdf()
            s_mqdf = "Prediction MQDF : " + name_result
            self.ori_text = self.ori_text+"\n"+s_mqdf
            self.panelB.config(text=self.ori_text)
            self.is_mqdf = False

    def exec_dlqdf(self):
        if self.panelB is not None and self.is_dlqdf:
            my_mq03 = Mq03.MainQdf03()
            my_mq03.set_index(self.idx)
            name_result = my_mq03.get_y_dlqdf()
            s_dlqdf = "Prediction DLQDF : " + name_result
            self.ori_text = self.ori_text + "\n" + s_dlqdf
            self.panelB.config(text=self.ori_text)
            self.is_dlqdf = False

    def gen_text(self, image_path):
        s_fl = "Filename : "
        fl = os.path.basename(image_path)
        s_y = "\n" + "Actual : "
        y = self.get_y_from_fl(fl)
        s_fl = s_fl + fl + s_y + y
        return s_fl

    def generate_gt_test_image_random(self, file_nm):
        pd_test = pd.read_csv(file_nm, delimiter=';', header=None, names=None, keep_default_na=False)
        self.l_fl_nm = pd_test.to_numpy(dtype=np.str)

    def get_y_from_fl(self, fl):
        result = np.argwhere(self.l_fl_nm == fl)
        self.idx = result[0][0]
        str = self.l_fl_nm[self.idx]
        rv = str[1]
        return rv


# Create a window and pass it to the Application object
App(tkinter.Tk(), "Testing ICHR Balinese with MQDF and DLQDF")
