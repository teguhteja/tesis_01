import pandas as pd
from pandas import DataFrame

f = open("header_03.txt", "r")
f1 = f.readlines()
header = f1[0].split(",")
header.append("label")
f.close()

data_training = pd.read_csv('X_test_01.csv', delimiter=',', header=None, names=header)
names_label = ['name_file','label'] # ditambah NAN karena file label test ada tambahan ;
label_training = pd.read_csv('balinese_gt_train.csv', delimiter=';', header=None, names=names_label)
group_name = label_training.label.unique()
data_training['label'] = label_training['label']

# split your data training
# l_filter_label = ["GANTUNGAN-LA","GANTUNGAN-GA","GU","GUWUNG", "KI","MA-TEDONG","SA-SAPA","WU","SA-SAGA", "RU",
#                     "NYA","DI","NING"]
# start_ind_col = 1984 #npwk
# end_ind_col = 2384
# data_training_1 = data_training.iloc[:, start_ind_col:end_ind_col]
# data_training_filter = data_training_1[data_training['label'].isin(l_filter_label)]
#
# y_train_filter1 = label_training[label_training['label'].isin(l_filter_label)]
# start_ind_y_col = 1
# end_ind_y_col = 2
# y_train_filter = y_train_filter1.iloc[:, start_ind_y_col:end_ind_y_col]
print(len(data_training))
# export_csv = data_training_filter.to_csv('X-training-split.csv', index=None, header=None)
export_csv = data_training.to_csv('X-testing-label_01.csv', index=None, header=None)
# export_csv = data_training.to_csv('X-training-split-npwk.csv', index=None, header=None)
# export_csv = data_training_filter.to_csv('X-training-split-npwk.csv', index=None)
# export_csv = y_train_filter.to_csv('y-training-split-npwk.csv', index=None)

